﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZICrypter.Communicator
{
    public class ZISettings
    {
        private const string Server = "160.99.38.140";
        private const string Port = "14330";
        private const string Username = "ZICrypt";
        private const string Password = "informacija204";
        private const string DBSchema = "ZICrypt";

        private static ZISettings _instance;
        private static readonly object Padlock = new object();
        public static ZISettings Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new ZISettings());
                }
            }
        }
        public string ConnectionString { get; protected set; }
        
        
        private ZISettings()
        {
            BuildConnectionString();
        }
        private void BuildConnectionString()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();
            builder.DataSource = $"{Server},{Port}";
            builder.UserID = Username;
            builder.Password = Password;
            builder.InitialCatalog = DBSchema;

            ConnectionString = builder.ToString();
        }


    }
}
