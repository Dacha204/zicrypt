﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZICrypter.Communicator.Model
{
    public class Client
    {
        public enum Status
        {
            Offline,
            Online
        }

        public int Id { get; set; }
        public string Username { get; set; }
        public Status OnlineStatus { get; set; }
        
    }
}
