﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZICrypter.Communicator.Model
{
    public class Message
    {
        public int Id { get; set; }
        public Client Sender { get; set; }
        public List<Client> Receivers { get; set; }
        public string MessageText { get; set; }

        public Message(Client sender)
        {
            Sender = sender;
            Receivers = new List<Client>();
        }
        public Message(Client sender, List<Client> receivers)
        {
            Sender = sender;
            Receivers = receivers;
        }
        
    }
}
