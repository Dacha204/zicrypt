﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZICrypter.Communicator.DBModel
{
    public class ZMessageReceivers
    {
        public int MessageID { get; set; }
        public int DestinationID { get; set; }
    }
}
