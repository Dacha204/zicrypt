﻿CREATE TABLE ZClients (
	ID int NOT NULL IDENTITY (1,1), 
	Username nvarchar(20),
	OnlineStatus int DEFAULT 0,

	CONSTRAINT PK_ZClients PRIMARY KEY NONCLUSTERED (ID),   
	CONSTRAINT AK_Username UNIQUE(Username)
);

CREATE TABLE ZMessages (
	ID int NOT NULL IDENTITY(1,1),
	SourceID int NOT NULL,
	MsgText nvarchar(MAX) NOT NULL,

	CONSTRAINT PK_ZMessages PRIMARY KEY NONCLUSTERED (ID),
	CONSTRAINT FK_ZMessages_ClientSource FOREIGN KEY (SourceID)
		REFERENCES ZClients (ID)   
		ON DELETE CASCADE  
		ON UPDATE CASCADE
);


CREATE TABLE ZMessageReceivers (
	MessageID int NOT NULL,
	DestinationID int NOT NULL,

	CONSTRAINT PK_ZMessageReceivers PRIMARY KEY NONCLUSTERED (MessageID, DestinationID),

	CONSTRAINT FK_ZMessageReceivers_ClientSource FOREIGN KEY (MessageID)
		REFERENCES ZMessages (ID)   
		ON DELETE CASCADE  
		ON UPDATE CASCADE,

	CONSTRAINT FK_ZMessageReceivers_ClientDestination FOREIGN KEY (DestinationID)
		REFERENCES ZClients (ID)   
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
);
