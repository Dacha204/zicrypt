﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZICrypter.Communicator.DBModel
{
    class ZClient
    {
        public int ID { get; set; }
        public string Username { get; set; }
        public int OnlineStatus { get; set; }
    }
}
