﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZICrypter.Communicator.DBModel
{
    class ZMessage
    {
        public int Id { get; set; }
        public int SourceId { get; set; }
        public string MessageText { get; set; }
    }
}
