﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TableDependency;
using TableDependency.Abstracts;
using TableDependency.EventArgs;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Where;
using ZICrypter.Communicator.DBModel;
using ZICrypter.Communicator.EventArg;
using ZICrypter.Communicator.Model;

namespace ZICrypter.Communicator
{
    public class ZICommunicator : IDisposable
    {
        #region Events
        public delegate void NewOnlineClientHandler(object sender, NewOnlineEventArgs e);
        public delegate void MessageReceivedHandler(object sender, MessageReceivedEventArgs e);

        public event NewOnlineClientHandler OnNewOnlineClient;
        public event MessageReceivedHandler OnNewMessage;

        #endregion

        #region Attributes

        private List<Client> _clients;
        private SqlTableDependency<ZClient> _clientDependency;
        private SqlTableDependency<ZMessageReceivers> _messageDependecy;
        
        #endregion

        #region Properties

        public Client Me { get; protected set; }
        public List<Client> Clients
        {
            get => _clients;
            protected set
            {
                _clients = value;
                _clients.RemoveAll(x => x.Id == Me.Id);
            }
        }

        #endregion

        #region Constructors

        public ZICommunicator(string username)
        {
            Me = RegisterClient(username);
            Clients = LoadAllClients();
            StartListening();
        }

        #endregion

        #region Methods
        public Message SendMessage(string message, List<Client> receivers)
        {
            using (var connection = new SqlConnection(ZISettings.Instance.ConnectionString))
            {
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"INSERT INTO [ZMessages] (SourceId, MsgText) VALUES " +
                                      $"({Me.Id},'{message}');";
                command.ExecuteNonQuery();

                command.CommandText = $"SELECT ID FROM [ZMessages] WHERE MsgText like '{message}';";
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleResult);
                reader.Read();
                int msgId = (int)reader[0];
                reader.Close();

                foreach (Client c in receivers)
                {
                    command.CommandText =
                        $"INSERT INTO [ZMessageReceivers] (MessageID, DestinationId) VALUES " +
                        $"({msgId},{c.Id});";

                    command.ExecuteNonQuery();
                }

                return new Message(Me, receivers)
                {
                    Id = msgId,
                    MessageText = message
                };
            }
        }

        private void StartListening()
        {
            _clientDependency = new SqlTableDependency<ZClient>(ZISettings.Instance.ConnectionString, "ZClients");
            _clientDependency.OnChanged += NewUserOnlineHandler;
            _clientDependency.Start();

            //GOPNIK DOES NOT SUPPORT p.DestinationID == Me.Id! BLYIN (Complex properties whatever)
            int meid = Me.Id;
            Expression<Func<ZMessageReceivers, bool>> filterExpression = p => p.DestinationID == meid;
            SqlTableDependencyFilter<ZMessageReceivers> filter = new SqlTableDependencyFilter<ZMessageReceivers>(filterExpression);

            _messageDependecy = new SqlTableDependency<ZMessageReceivers>(
                connectionString: ZISettings.Instance.ConnectionString, 
                tableName: "ZMessageReceivers",
                filter: filter
            );
            
            _messageDependecy.OnChanged += NewMessageHandler;
            _messageDependecy.Start();
        }
        
        private void NewUserOnlineHandler(object sender, RecordChangedEventArgs<ZClient> e)
        {
            ZClient changedEntity = e.Entity;

            //Console.WriteLine();
            //Console.WriteLine("\t==== New Online User Handler ====");
            //Console.WriteLine("\tDML operation: " + e.ChangeType);
            //Console.WriteLine("\tID: " + changedEntity.ID);
            //Console.WriteLine("\tUsername: " + changedEntity.Username);
            //Console.WriteLine("\tOnlineStatus: " + changedEntity.OnlineStatus);

            var c = new Client()
            {
                Id = changedEntity.ID,
                Username = changedEntity.Username,
                OnlineStatus = (Client.Status) changedEntity.OnlineStatus
            };

            if (c.OnlineStatus == Client.Status.Offline)
                Clients.RemoveAll(x => x.Id == c.Id);
            else
                Clients.Add(c);

            var eventArgs = new NewOnlineEventArgs()
            {
                OnlineClients = this.Clients,
                NewOnline = c
            };


            //Console.WriteLine("\tOnline Clients: " + Clients.Count);
            //Console.WriteLine("\t==================================");

            OnNewOnlineClient?.Invoke(this, eventArgs);
            
        }
        private void NewMessageHandler(object sender, RecordChangedEventArgs<ZMessageReceivers> e)
        {
            ZMessageReceivers changedEntity = e.Entity;

            //Console.WriteLine();
            //Console.WriteLine("\t==== New Message Handler ====");
            //Console.WriteLine("\tDML operation: " + e.ChangeType);
            //Console.WriteLine("\tMessageId: " + changedEntity.MessageID);
            //Console.WriteLine("\tDestinationId: " + changedEntity.DestinationID);

            var message = LoadMessage(changedEntity.MessageID);
            var eventArgs = new MessageReceivedEventArgs()
            {
                Message = message
            };

            //Console.WriteLine("\tMessage: " + message.MessageText);
            //Console.WriteLine("\t==================================");
            OnNewMessage?.Invoke(this, eventArgs);
        }

        public void Dispose()
        {
            _clientDependency?.Stop();
            _messageDependecy?.Stop();

            _clientDependency?.Dispose();
            _messageDependecy?.Dispose();

            Me.OnlineStatus = Client.Status.Offline;
            UpdateUserStatus(Client.Status.Offline);
        }
        
        protected List<Client> LoadAllClients()
        {
            using (var connection = new SqlConnection(ZISettings.Instance.ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT * FROM [ZClients] WHERE OnlineStatus = {(int)Client.Status.Online};";

                connection.Open();
                SqlDataReader reader = command.ExecuteReader();

                var clients = new List<Client>();
                while (reader.Read())
                {
                    var c = new Client
                    {
                        Id = (int)reader["Id"],
                        Username = reader["Username"].ToString(),
                        OnlineStatus = (int)reader["OnlineStatus"] == 1 ? Client.Status.Online : Client.Status.Offline,
                    };
                    clients.Add(c);
                }

                return clients;
            }
        }
        protected Client LoadClient(string username)
        {
            using (var connection = new SqlConnection(ZISettings.Instance.ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT * FROM [ZClients] WHERE Username like '{username}';";

                connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow);
                reader.Read();

                return new Client
                {
                    Id = (int)reader["Id"],
                    Username = reader["Username"].ToString(),
                    OnlineStatus = (int)reader["OnlineStatus"] == 1 ? Client.Status.Online : Client.Status.Offline,
                };
            }
        }
        protected Client RegisterClient(string username)
        {
            if (UserExists(username))
                return LoadClient(username);

            using (var connection = new SqlConnection(ZISettings.Instance.ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"INSERT INTO [ZClients] (Username, OnlineStatus) VALUES " +
                                      $"('{username}', {(int) Client.Status.Online});";

                connection.Open();
                command.ExecuteNonQuery();
            }

            return LoadClient(username);
        }
        protected bool UserExists(string username)
        {
            using (var connection = new SqlConnection(ZISettings.Instance.ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT COUNT(*) FROM [ZClients] WHERE Username like '{username}';";

                connection.Open();
                int clientCount = (int)command.ExecuteScalar();
                return clientCount == 1;
            }
        }

        protected void UpdateUserStatus(Client.Status status)
        {
            using (SqlConnection connection = new SqlConnection(ZISettings.Instance.ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText =
                    $"UPDATE [ZClients] SET OnlineStatus = {(int)status} WHERE Username like '{Me.Username}';";

                connection.Open();
                command.ExecuteNonQuery();
            }
        }
        protected Message LoadMessage(int id)
        {
            using (var connection = new SqlConnection(ZISettings.Instance.ConnectionString))
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = $"SELECT * FROM [ZMessages] WHERE ID like '{id}';";

                connection.Open();
                SqlDataReader reader = command.ExecuteReader(CommandBehavior.SingleRow);
                reader.Read();

                Client sender = Clients.Find(x => x.Id == (int) reader["SourceID"]);
                var msg = new Message(sender)
                {
                    Id = (int)reader["ID"],
                    MessageText = (string)reader["MsgText"],
                };

                return msg;
            }
        }
        #endregion
    }
}