﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.Communicator.Model;

namespace ZICrypter.Communicator.EventArg
{
    public class NewOnlineEventArgs : EventArgs
    {
        public List<Client> OnlineClients { get; set; }
        public Client NewOnline { get; set; }
    }
}
