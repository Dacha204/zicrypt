﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace ZICrypter.FSW
{
    [DataContract]
    public class FileCryptRequest
    {
        public enum Status
        {
            Blank,
            Pending,
            Done
        }

        private FileInfo _sourceFile;

        [DataMember] public FileInfo File
        {
            get => _sourceFile;
            set
            {
                if (value == null || !value.Exists)
                    throw new ArgumentException("FileCryptRequest: Can't add non-existing file to FileCryptRequest!");
                    
                _sourceFile = value;
                DestinationDirectory = value.Directory.FullName;
                DestinationName = value.Name + ".crypt";
                RequestStatus = Status.Pending;
            }
        }
        
        public FileCryptRequest(FileInfo sourceFile)
        {
            File = sourceFile;
        }
 
        [DataMember] public string DestinationDirectory { get; set; }
        [DataMember] public string DestinationName { get; set; }
        public string DestinationFullName => DestinationDirectory + "\\" + DestinationName;
        [DataMember] public Status RequestStatus;
    }
}
