﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Serialization;
using System.Threading;
using System.Xml;
using ZICrypter.CryptoLib.CryptoHandlers;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.FSW.EventArgs;

namespace ZICrypter.FSW
{
    public class FSWCrytper
    {
        #region Events

        public delegate void FSWCrypterEventHandler(object sender, FSWEventArg e);

        public event FSWCrypterEventHandler OnFSWEvent;
        private void Notify(FSWEventTypeEnum type, string message)
        {
            var eventArgs = new FSWEventArg()
            {
                EventType = type,
                Message = message
            };

            OnFSWEvent?.Invoke(this, eventArgs);
        }
        #endregion

        #region Static Methods, Enum, Const

        public enum FSWState
        {
            Initialization,
            Running,
            Stopped
        }
        public const string DefaultFSWSaveFile = "FSWState.xml";
        public const string EncryptedExtension = ".crypt";
        public const int MaxActiveThreads = 5;

        private static bool FSWIsFileExcluded(FileInfo file)
        {
            return file.Extension == EncryptedExtension;
        }
        private static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.OpenOrCreate, FileAccess.ReadWrite);
            }
            catch (IOException)
            {
                return true;
            }
            finally
            {
                stream?.Close();
            }

            return false;
        }

        #endregion

        #region Members

        private FileSystemWatcher _watcher;

        private string _sourceDir;
        private string _destDir;

        protected EncryptionHandler Encrypter;
        protected DecryptionHandler Decrypter;

        protected ConcurrentQueue<FileCryptRequest> PendingRequestsBuffer;
        protected List<FileCryptRequest> FinishedRequests;

        private int AvailableThreads { get; set; }
        private object _availableThreadsLock = new object();

        #endregion

        #region Properties

        public string SourceDir
        {
            get => _sourceDir;
            set
            {
                if (!Directory.Exists(value))
                    throw new ArgumentException("FSWCrytper: Source directory does not exists");
                
                if (State == FSWState.Running)
                    throw new Exception("FSWacher: Can not change source directory while running");

                _sourceDir = value;
            }
        }
        public string DestinationDir
        {
            get => _destDir;
            set
            {
                if (!Directory.Exists(value))
                    throw new ArgumentException("FSWCrytper: Destination directory does not exists");

                if (State == FSWState.Running)
                    throw new Exception("FSWacher: Can not change source directory while running");

                _destDir = value;
            }
        }
        public FSWState State { get; private set; }
        
        #endregion

        #region Constructors

        private void InitFSWState(string sourceDir, string destinationDir)
        {
            State = FSWState.Initialization;
            SourceDir = sourceDir;
            DestinationDir = destinationDir;
            PendingRequestsBuffer = new ConcurrentQueue<FileCryptRequest>();
            AvailableThreads = MaxActiveThreads;
            FinishedRequests = new List<FileCryptRequest>();
            
            lock (FinishedRequests)
            {
                FinishedRequests = new List<FileCryptRequest>();
            }
        }
        private void InitDefaultCryptoHandlers()
        {
            SimpleSubstitutionKey key = new SimpleSubstitutionKey();
            for (int i = 0; i < 256; i++)
            {
                key.SubstitutionMap.Add((byte)i, (byte)~i);
            }

            Encrypter = new EncryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution)
            {
                Key = key
            };
            Decrypter = new DecryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution)
            {
                Key = key
            };
        }

        public FSWCrytper(string sourceDir)
        {
            InitFSWState(sourceDir, sourceDir);
            InitDefaultCryptoHandlers();
        }
        public FSWCrytper(string sourceDir, string destinationDir)
        {
            InitFSWState(sourceDir, destinationDir);
            InitDefaultCryptoHandlers();
        }
        public FSWCrytper(string sourceDir, string destDir, EncryptionHandler encrypter, DecryptionHandler decrypter)
        {
            if (encrypter.Key == null)
                throw new ArgumentException("FSW Crypter: encrypter key is not initialized!");
            if (decrypter.Key == null)
                throw new ArgumentException("FSW Crypter: decrypter key is not initialized!");


            InitFSWState(sourceDir, destDir);
            Encrypter = encrypter;
            Decrypter = decrypter;
        }
        
        #endregion

        #region Methods

        public void Save()
        {
            Notify(FSWEventTypeEnum.SavingFSWState, "Saving FSW state...");

            lock (FinishedRequests)
            {
                FSWSettings settings = new FSWSettings
                {
                    FinishedRequests = FinishedRequests,
                    SourceDirectory = SourceDir,
                    DestinationDirectory = DestinationDir
                };

                settings.SaveToFile(DefaultFSWSaveFile);
            }

            Notify(FSWEventTypeEnum.SavedFSWState, "FSW state saved.");
        }
        public void Restore()
        {
            if (State == FSWState.Running)
                throw new Exception("FSWatcher: Can not restore settings while running");

            Notify(FSWEventTypeEnum.RestoringFSWState, "Restoring FSW state...");

            try
            {

                FSWSettings settings = FSWSettings.LoadFromFile(DefaultFSWSaveFile);

                SourceDir = settings.SourceDirectory;
                DestinationDir = settings.DestinationDirectory;
                
                FinishedRequests = settings.FinishedRequests;
            }
            catch (Exception)
            {
                // ignored
            }
            finally
            {
                Notify(FSWEventTypeEnum.RestoredFSWState, $"Restored {FinishedRequests.Count}");
            }
        }
        public void Start()
        {
            if (State == FSWState.Running)
                throw new Exception("FSWatcher: Already running");

            _watcher = new FileSystemWatcher(SourceDir)
            {
                Filter = "*",
                NotifyFilter =
                  NotifyFilters.LastAccess
                | NotifyFilters.LastWrite
                | NotifyFilters.FileName
            };
            _watcher.Created += FSWOnChange;
            _watcher.EnableRaisingEvents = true;
            State = FSWState.Running;

            Thread requestProcessorThread = new Thread(RequestsProcessing);
            requestProcessorThread.Start();

            Notify(FSWEventTypeEnum.FSWStarted, 
                $"FSW Started. {Environment.NewLine}" +
                $"SourceDir {SourceDir} {Environment.NewLine}" +
                $"DestinationDir {SourceDir} {Environment.NewLine}");
        }
        public void Stop()
        {
            if (State == FSWState.Initialization)
                throw new Exception("FSWatcher: Can't stop watcher. Watcher hasn't been started previously.");

            State = FSWState.Stopped;
            _watcher.EnableRaisingEvents = false;

            Notify(FSWEventTypeEnum.FSWStopped, "FSW Stopped.");
        }

        public void StartRestored()
        {
            Restore();
            Start();
        }
        public void StopAndSave()
        {
            Stop();
            Save();
        }
        
        #endregion
        
        #region FSW_Handlers

        private void FSWOnChange(object source, FileSystemEventArgs e)
        {
            FileInfo file = new FileInfo(e.FullPath);

            if (FSWIsFileExcluded(file))
                return;

            Notify(FSWEventTypeEnum.NewFileDetected,
                $"New file detected: {e.FullPath}");

            FileCryptRequest request = new FileCryptRequest(file)
            {
                DestinationDirectory = DestinationDir
            };
 
            PendingRequestsBuffer.Enqueue(request);
        }
        private void RequestsProcessing()
        {
            Notify(FSWEventTypeEnum.FSWStarted, "FSW Requests processing started");
            
            while (State == FSWState.Running)
            {
                while (PendingRequestsBuffer.IsEmpty && State == FSWState.Running)
                    Thread.Sleep(100);

                if (State == FSWState.Stopped) break;

                FileCryptRequest request;
                while (!PendingRequestsBuffer.TryDequeue(out request) && State == FSWState.Running)
                    Thread.Sleep(20);

                if (State == FSWState.Stopped) break;

                Thread cryptingThread = new Thread(() => CryptFile(request));
                while (FSWThreading.AvailableThreads <= 0 && State == FSWState.Running)
                    Thread.Sleep(10);

                if (State == FSWState.Stopped) break;

                cryptingThread.Start();
            }
            
            Notify(FSWEventTypeEnum.FSWStopped, "FSW Requests processing stopped.");
        }
        private void CryptFile(FileCryptRequest request)
        {
            FSWThreading.ConsumeThread();

            Notify(FSWEventTypeEnum.StartedEncrypting,
                $"Encrypting {request.File.FullName}...");
            
            while (IsFileLocked(request.File)) //potential infinity waiting
                Thread.Sleep(100);

            Stopwatch watch = new Stopwatch();
            watch.Restart();

            Encrypter.PerformCrypt(request.File.FullName, request.DestinationFullName);
            request.File.Delete();
            
            request.RequestStatus = FileCryptRequest.Status.Done;

            lock (FinishedRequests)
            {
                FinishedRequests.Add(request);
            }

            Notify(FSWEventTypeEnum.FinishedEncrypting,
                $"Finished {request.File.FullName} encrypting. Encryption time: {watch.ElapsedMilliseconds} ms");
            
            FSWThreading.ReleaseThread();
        }

        public void DecryptAll()
        {
            Notify(FSWEventTypeEnum.StartedDecrypting, "== Decryption of all files started! ==");
            lock (FinishedRequests)
            {
                string destDir = DestinationDir + "\\Decrypted\\";
                if (!Directory.Exists(destDir))
                    Directory.CreateDirectory(destDir);

                foreach (FileCryptRequest request in FinishedRequests)
                {

                    Notify(FSWEventTypeEnum.StartedDecrypting,
                        $"Decrypting {request.File.FullName}...");
                    
                    string source = request.DestinationFullName;
                    string dest = destDir + request.File.Name;

                    Stopwatch watch = new Stopwatch();
                    watch.Restart();

                    Decrypter.PerformCrypt(source, dest);

                    watch.Stop();

                    Notify(FSWEventTypeEnum.FinishedDecrypting,
                        $"Finished {request.File.FullName} decrypting. Decryption time: {watch.ElapsedMilliseconds} ms");

                    File.Delete(request.DestinationFullName);
                }
                FinishedRequests.Clear();
                Save();
            }

            Notify(FSWEventTypeEnum.FinishedDecrypting, "== Decryption of all files finished! ==");

        }
        #endregion

        #region FSWCrypterSettingsClass

        [DataContract]
        private class FSWSettings
        {
            [DataMember] public List<FileCryptRequest> FinishedRequests { get; set; }
            [DataMember] public string SourceDirectory { get; set; }
            [DataMember] public string DestinationDirectory { get; set; }

            public void SaveToFile(string filename)
            {
                SaveToFile(filename, this);
            }

            public static void SaveToFile(string filename, FSWSettings obj)
            {
                var serializer = new DataContractSerializer(typeof(FSWSettings));
                var stringWriter = new StringWriter();
                var xmlWriter = new XmlTextWriter(stringWriter)
                {
                    Formatting = Formatting.Indented
                };
                var writer = new StreamWriter(filename);

                try
                {
                    serializer.WriteObject(xmlWriter, obj);
                    writer.Write(stringWriter.ToString());
                }
                catch (Exception ex)
                {
                    throw new SerializationException("Saving FSW settings failed!", ex);
                }
                finally
                {
                    stringWriter.Close();
                    xmlWriter.Close();
                    writer.Close();
                }
            }
            public static FSWSettings LoadFromFile(string filename)
            {
                var deserializer = new DataContractSerializer(typeof(FSWSettings));
                XmlTextReader reader = new XmlTextReader(filename);

                FSWSettings obj;

                try
                {
                    obj = (FSWSettings) deserializer.ReadObject(reader);
                }
                catch (Exception ex)
                {
                    throw new SerializationException("Loading FSW settings failed!", ex);
                }
                finally
                {
                    reader.Close();
                }

                return obj;
            }
        }

        private static class FSWThreading
        {
            private static readonly object AvailableThreadsLock = new object();
            public static int AvailableThreads { get; private set; } = MaxActiveThreads;

            public static void ConsumeThread()
            {
                lock (AvailableThreadsLock)
                {
                    AvailableThreads--;
                }
            }
            public static void ReleaseThread()
            {
                lock (AvailableThreadsLock)
                {
                    AvailableThreads++;
                }
            }
        }
        #endregion
    }
}