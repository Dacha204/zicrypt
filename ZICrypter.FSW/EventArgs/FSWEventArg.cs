﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZICrypter.FSW.EventArgs
{
    public class FSWEventArg
    {
        public FSWEventTypeEnum EventType { get; set; }
        public string Message { get; set; }
    }
}
