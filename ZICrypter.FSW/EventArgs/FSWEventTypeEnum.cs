﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZICrypter.FSW.EventArgs
{
    public enum FSWEventTypeEnum
    {
        FSWStarted,
        FSWStopped,

        NewFileDetected,

        StartedEncrypting,
        FinishedEncrypting,

        StartedDecrypting,
        FinishedDecrypting,

        SavingFSWState,
        SavedFSWState,

        RestoringFSWState,
        RestoredFSWState,
    }
}
