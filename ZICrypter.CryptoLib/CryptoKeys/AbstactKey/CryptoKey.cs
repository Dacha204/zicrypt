﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

namespace ZICrypter.CryptoLib.CryptoKeys.AbstactKey
{
    [DataContract]
    public abstract class CryptoKey
    {
        public enum ParseMode
        {
            ValidateOnly,
            LoadParsed
        }
        
        public bool Validate(string key)
        {
            try
            {
                ParseKey(key, ParseMode.ValidateOnly);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public abstract void ParseKey(string key, ParseMode mode = ParseMode.LoadParsed);

        public abstract void SaveToFile(string filename);
        public abstract void LoadFromFile(string filename);

        protected static void SaveToFile(object obj, string filename)
        {
            DataContractSerializer serializer = new DataContractSerializer(obj.GetType());
            StringWriter stringWriter = new StringWriter();
            XmlTextWriter xmlWriter = new XmlTextWriter(stringWriter);
            StreamWriter streamWriter = new StreamWriter(filename);

            try
            {
                xmlWriter.Formatting = Formatting.Indented;

                serializer.WriteObject(xmlWriter, obj);
                streamWriter.Write(stringWriter.ToString());
            }
            catch (Exception ex)
            {
                throw new SerializationException("Saving key to file failed", ex);
            }
            finally
            {
                stringWriter.Close();
                xmlWriter.Close();
                streamWriter.Close();
            }
            
        }
        protected static object LoadFromFile(System.Type type, string filename)
        {
            var deserializer = new DataContractSerializer(type);
            XmlTextReader reader = new XmlTextReader(filename);
            var obj = deserializer.ReadObject(reader);

            return obj;
        }


    }
}
