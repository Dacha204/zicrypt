﻿using System.Runtime.Serialization;

namespace ZICrypter.CryptoLib.CryptoKeys.AbstactKey
{
    [DataContract]
    public abstract class AsymetricCryptoKey : CryptoKey
    {
    }
}
