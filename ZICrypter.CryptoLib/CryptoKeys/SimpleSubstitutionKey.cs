﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.CryptoKeys
{
    [DataContract]
    public class SimpleSubstitutionKey : SymetricCrytoKey
    {
        [DataMember]
        public Dictionary<byte, byte> SubstitutionMap { get; set; }
        
        public SimpleSubstitutionKey()
        {
            SubstitutionMap = new Dictionary<byte, byte>();
        }

        public override void ParseKey(string key, ParseMode mode = ParseMode.LoadParsed)
        {
            char[] delimeters = {'|'};
            string[] parts = key.Split(delimeters,StringSplitOptions.RemoveEmptyEntries);

            Dictionary<byte,byte> newDictionary = new Dictionary<byte, byte>();
            Dictionary<byte, byte> reverseDictionary = new Dictionary<byte, byte>();
            try
            {
                foreach (string t in parts)
                {
                    if (t.Length != 2)
                        throw new Exception($"Replacement for {t[0]} has to be 1 char");

                    char left = t[0];
                    char right = t[1];
                    newDictionary.Add((byte)left, (byte)right);
                    reverseDictionary.Add((byte)right, (byte) left);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("SimpleSubstitutionKey: Failed to parse string. " + ex.Message, ex);
            }

            if (mode == ParseMode.LoadParsed)
                SubstitutionMap = newDictionary;
        }
        public override void SaveToFile(string filename)
        {
            SaveToFile(this, filename);
        }
        public override void LoadFromFile(string filename)
        {
            SimpleSubstitutionKey key = LoadFromFile(this.GetType(), filename) as SimpleSubstitutionKey;
            this.SubstitutionMap = key.SubstitutionMap;
        }

        public override string ToString()
        {
            string result = String.Empty;
            foreach (byte mapKey in SubstitutionMap.Keys)
            {
                byte mapValue = SubstitutionMap[mapKey];

                string key = Convert.ToChar(mapKey).ToString();
                string value = Convert.ToChar(mapValue).ToString();

                result += $"{key}{value}|";
            }
            result.Remove(result.Length - 1);
            return result;
        }
    }
}
