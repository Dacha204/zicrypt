﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.CryptoKeys
{
    [DataContract]
    public class A51Key : SymetricCrytoKey
    {
        [DataMember] public BitArray RegistryX { get; set; }
        [DataMember] public BitArray RegistryY { get; set; }
        [DataMember] public BitArray RegistryZ { get; set; }
        [DataMember] public int MajorityBitX { get; set; }
        [DataMember] public int MajorityBitY { get; set; }
        [DataMember] public int MajorityBitZ { get; set; }
        [DataMember] public int[] ShiftGeneratorsX { get; set; }
        [DataMember] public int[] ShiftGeneratorsY { get; set; }
        [DataMember] public int[] ShiftGeneratorsZ { get; set; }

        public A51Key()
        {
            RegistryX = new BitArray(19);
            RegistryY = new BitArray(22);
            RegistryZ = new BitArray(23);

            MajorityBitX = 8;
            MajorityBitY = 10;
            MajorityBitZ = 10;

            ShiftGeneratorsX = new int[] {13,16,17,18};
            ShiftGeneratorsY = new int[] {20,21};
            ShiftGeneratorsZ = new int[] {7,20,21,22};
        }

        //Format: "<64 bit registry number>|<MajorityX>;<MajorityY>;<MajorityZ>|<ShiftGeneratorBitX>,<ShiftGeneratorBitX>;<..Y>;<..Z"
        public override void ParseKey(string keyString, ParseMode mode = ParseMode.LoadParsed)
        {
            string[] sections = keyString.Split('|');
            
            if (sections.Length < 3)
                throw new Exception("ParseA51Key: Invalid number of section |");

            BitArray newRegistryX;
            BitArray newRegistryY;
            BitArray newRegistryZ;
            int newMajorityBitX;
            int newMajorityBitY;
            int newMajorityBitZ;
            int[] newShiftersX;
            int[] newShiftersY;
            int[] newShiftersZ;

            #region Registries
            try
            {
                ulong number = ulong.Parse(sections[0]);
                ulong maskIndex = 0x8000000000000000;

                newRegistryX = ParseRegistry(number, ref maskIndex, 19);
                newRegistryY = ParseRegistry(number, ref maskIndex, 22);
                newRegistryZ = ParseRegistry(number, ref maskIndex, 23);
            }
            catch (Exception ex)
            {
                throw new Exception("ParseA51Key: Failed to parse 64bit number. Details:" + ex.Message);
            }
            #endregion
            #region Majority
            try
            {
                string[] majorities = sections[1].Split(';');
                newMajorityBitX = int.Parse(majorities[0]);
                newMajorityBitY = int.Parse(majorities[1]);
                newMajorityBitZ = int.Parse(majorities[2]);
            }
            catch (Exception ex)
            {
                throw new Exception("ParseA51Key: Failed to parse majority bits. Details:" + ex.Message);
            }
            #endregion
            #region ShiftGenerators
            try
            {
                string[] shifters = sections[2].Split(';');

                if (shifters.Length != 3)
                    throw new Exception("ParseA51Key: Invalid arguments for ShiftGenerators.");

                newShiftersX = ParseShiftGenerators(shifters[0]);
                newShiftersY = ParseShiftGenerators(shifters[1]);
                newShiftersZ = ParseShiftGenerators(shifters[2]);
            }
            catch (Exception ex)
            {
                throw new Exception("ParseA51Key: Failed to parse shift generators bits. Details:" + ex.Message);
            }
            #endregion
            
            //TODO: Refactor in separate function
            if (newMajorityBitX > 18)
                throw new ArgumentException("ParseA51Key validation error: MajorityBitX is over 18!");
            if (newMajorityBitY > 22)
                throw new ArgumentException("ParseA51Key validation error: MajorityBitY is over 22!");
            if (newMajorityBitZ > 23)
                throw new ArgumentException("ParseA51Key validation error: MajorityBitZ is over 23!");

            if (newShiftersX.Any(i => i > 18))
                throw new ArgumentException("parseA51Key validation error: ShiftGeneratorBitX is over 18!");
            if (newShiftersY.Any(i => i > 22))
                throw new ArgumentException("parseA51Key validation error: ShiftGeneratorBitY is over 22!");
            if (newShiftersZ.Any(i => i > 23))
                throw new ArgumentException("parseA51Key validation error: ShiftGeneratorBitZ is over 23!");
            
            if (mode == ParseMode.ValidateOnly)
                return;

            RegistryX = newRegistryX;
            RegistryY = newRegistryY;
            RegistryZ = newRegistryZ;
            MajorityBitX = newMajorityBitX;
            MajorityBitY = newMajorityBitY;
            MajorityBitZ = newMajorityBitZ;
            ShiftGeneratorsX = newShiftersX;
            ShiftGeneratorsY = newShiftersY;
            ShiftGeneratorsZ = newShiftersZ;
        }
        private BitArray ParseRegistry(ulong number, ref ulong mask, int length)
        {
            BitArray parsedArray = new BitArray(length);
            for (int i = 0; i < length; i++)
            {
                ulong extracted = number & mask;
                parsedArray[i] = extracted > 0;
                mask = mask >> 1;
            }

            return parsedArray;
        }
        private int[] ParseShiftGenerators(string parseString)
        {
            string[] shifters = parseString.Split(',');
            int[] parsedShifters = new int[shifters.Length];

            for (int i = 0; i < shifters.Length; i++)
            {
                parsedShifters[i] = int.Parse(shifters[i]);
            }

            return parsedShifters;
        }
        
        public override void SaveToFile(string filename)
        {
            SaveToFile(this,filename);
        }
        public override void LoadFromFile(string filename)
        {
            A51Key key = LoadFromFile(this.GetType(), filename) as A51Key;
            RegistryX = key.RegistryX;
            RegistryY = key.RegistryY;
            RegistryZ = key.RegistryZ;

            MajorityBitX = key.MajorityBitX;
            MajorityBitY = key.MajorityBitY;
            MajorityBitZ = key.MajorityBitZ;

            ShiftGeneratorsX = key.ShiftGeneratorsX;
            ShiftGeneratorsY = key.ShiftGeneratorsY;
            ShiftGeneratorsZ = key.ShiftGeneratorsZ;

        }

        private ulong GenerateRegisterNumber()
        {
            ulong maskIndex = 0x8000000000000000;
            ulong generated = 0;

            for (int i = 0; i < 19; i++)
            {
                if (RegistryX[i] == true)
                    generated |= maskIndex;
                maskIndex = maskIndex >> 1;
            }
            for (int i = 0; i < 22; i++)
            {
                if (RegistryY[i] == true)
                    generated |= maskIndex;
                maskIndex = maskIndex >> 1;
            }
            for (int i = 0; i < 23; i++)
            {
                if (RegistryZ[i] == true)
                    generated |= maskIndex;
                maskIndex = maskIndex >> 1;
            }
            return generated;
        }
        public override string ToString()
        {
            ulong registryNum = GenerateRegisterNumber();

            string result = $"{registryNum}|{MajorityBitX};{MajorityBitY};{MajorityBitZ}|";

            result = ShiftGeneratorsX.Aggregate(result, (current, x) => current + $"{x},");
            result = result.Remove(result.Length - 1) + ";";

            result = ShiftGeneratorsY.Aggregate(result, (current, y) => current + $"{y},");
            result = result.Remove(result.Length - 1) + ";";

            result = ShiftGeneratorsZ.Aggregate(result, (current, z) => current + $"{z},");
            result = result.Remove(result.Length - 1);

            return result;
        }
    }
}
