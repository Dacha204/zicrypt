﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.CryptoKeys
{
    [DataContract]
    public class RSAPrivateKey : PrivateCryptoKey
    {
        [DataMember]
        public ulong D { get; set; }
        [DataMember]
        public ulong N { get; set; }


        public override void ParseKey(string key, ParseMode mode = ParseMode.LoadParsed)
        {
            throw new NotImplementedException();
        }

        public override void SaveToFile(string filename)
        {
            SaveToFile(this, filename);
        }
        public override void LoadFromFile(string filename)
        {
            RSAPrivateKey key = LoadFromFile(this.GetType(), filename) as RSAPrivateKey;
            D = key.D;
            N = key.N;
        }
    }
}
