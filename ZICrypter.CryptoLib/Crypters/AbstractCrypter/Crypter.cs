﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.Crypters.AbstractCrypter
{
    abstract class Crypter
    {
        private CryptoKey _key;

        public CryptoKey Key
        {
            get => _key;
            set
            {
                if (!ValidateKeyType(value))
                    throw new ArgumentException("Invalid key type used for crypter!");

                _key = value;
                InitCrypto();
            }

        }

        protected abstract bool ValidateKeyType(CryptoKey key);
        protected abstract void InitCrypto();
    }
}
