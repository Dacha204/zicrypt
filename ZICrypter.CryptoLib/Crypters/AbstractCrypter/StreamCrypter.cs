﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.Crypters.Interfaces;

namespace ZICrypter.CryptoLib.Crypters.AbstractCrypter
{
    abstract class StreamCrypter : SymetricCrypter, IStreamCrypto
    {
        public virtual StreamCryptoMode Mode { get; set; }

        protected StreamCrypter()
        {
            Mode = StreamCryptoMode.Disabled;
        }

    }
}
