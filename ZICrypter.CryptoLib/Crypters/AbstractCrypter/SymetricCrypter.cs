﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.Crypters.Interfaces;

namespace ZICrypter.CryptoLib.Crypters.AbstractCrypter
{
    abstract class SymetricCrypter : Crypter, IEncrypter, IDecrypter
    {
        public abstract byte[] Encrypt(byte[] bytes);
        public abstract byte[] Decrypt(byte[] bytes);

    }
}
