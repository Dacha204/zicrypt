﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.Crypters.AbstractCrypter;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.Crypters
{
    class RSAEncrypter : Encrypter
    {
        protected override bool ValidateKeyType(CryptoKey key)
        {
            return key is RSAPublicKey;
        }

        protected override void InitCrypto()
        {
            throw new NotImplementedException();
        }

        public override byte[] Encrypt(byte[] bytes)
        {
            throw new NotImplementedException();
        }
    }
}
