﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.Crypters.AbstractCrypter;
using ZICrypter.CryptoLib.Crypters.Interfaces;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.Crypters
{
    class A51Crypter : StreamCrypter
    {
        private static void ShiftRegistry(bool[] registry, bool insertBit)
        {
            for (int i = registry.Length - 1; i > 0; i--)
            {
                registry[i] = registry[i - 1];
            }
            registry[0] = insertBit;
        }
        private static bool GetShiftInsertBit(bool[] registry, int[] bitsPositions)
        {
            bool insertBit = false;
            foreach (int bitPosition in bitsPositions)
            {
                insertBit ^= registry[bitPosition];
            }

            return insertBit;
        }

        private bool[] _registryX;
        private bool[] _registryY;
        private bool[] _registryZ;

        private int _majorityBitX;
        private int _majorityBitY;
        private int _majorityBitZ;

        private int[] _shiftGeneratorsX;
        private int[] _shiftGeneratorsY;
        private int[] _shiftGeneratorsZ;

        private bool MajorityVote()
        {
            bool x = _registryX[_majorityBitX];
            bool y = _registryY[_majorityBitY];
            bool z = _registryZ[_majorityBitZ];

            return x & y | x & z | y & z;
        }
        private void ShiftRegistries()
        {
            bool major = MajorityVote();
            bool insertBit;

            //Registry X
            if (_registryX[_majorityBitX] == major)
            {
                insertBit = GetShiftInsertBit(_registryX, _shiftGeneratorsX);
                ShiftRegistry(_registryX, insertBit);
            }
            //Registry Y
            if (_registryY[_majorityBitY] == major)
            {
                insertBit = GetShiftInsertBit(_registryY, _shiftGeneratorsY);
                ShiftRegistry(_registryY, insertBit);
            }
            //Registry Z
            if (_registryZ[_majorityBitZ] == major)
            {
                insertBit = GetShiftInsertBit(_registryZ, _shiftGeneratorsZ);
                ShiftRegistry(_registryZ, insertBit);
            }
        }
        private byte GetKeystreamByte()
        {
            byte KeyStreamByte = 0;
            bool KeyStreamBit;

            for (int i = 0; i < 8; i++)
            {
                KeyStreamBit =
                    _registryX[_registryX.Length - 1] ^
                    _registryY[_registryY.Length - 1] ^
                    _registryZ[_registryZ.Length - 1];

                KeyStreamByte = (byte) ((KeyStreamByte << 1) | (KeyStreamBit ? 1 : 0));
                ShiftRegistries();
            }
            return KeyStreamByte;
        }
        private void ExecuteCryptoOperation(byte[] bytes)
        {
            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] ^= GetKeystreamByte();
            }

            if (Mode == StreamCryptoMode.Disabled)
                InitCrypto();
        }
        
        protected override bool ValidateKeyType(CryptoKey key)
        {
            return key is A51Key;
        }
        protected override void InitCrypto()
        {
            A51Key key = Key as A51Key;

            _registryX = new bool[key.RegistryX.Length];
            _registryY = new bool[key.RegistryY.Length];
            _registryZ = new bool[key.RegistryZ.Length];

            for (int i = 0; i < key.RegistryX.Length; i++)
                _registryX[i] = key.RegistryX[i];

            for (int i = 0; i < key.RegistryY.Length; i++)
                _registryY[i] = key.RegistryY[i];

            for (int i = 0; i < key.RegistryZ.Length; i++)
                _registryZ[i] = key.RegistryZ[i];
            
            _majorityBitX = key.MajorityBitX;
            _majorityBitY = key.MajorityBitX;
            _majorityBitZ = key.MajorityBitZ;

            _shiftGeneratorsX = key.ShiftGeneratorsX;
            _shiftGeneratorsY = key.ShiftGeneratorsY;
            _shiftGeneratorsZ = key.ShiftGeneratorsZ;
        }

        public override byte[] Encrypt(byte[] bytes)
        {
            ExecuteCryptoOperation(bytes);
            return bytes;
        }
        public override byte[] Decrypt(byte[] bytes)
        {
            ExecuteCryptoOperation(bytes);
            return bytes;
        }
    }
}