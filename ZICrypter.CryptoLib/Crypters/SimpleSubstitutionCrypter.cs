﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.Crypters.AbstractCrypter;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.Crypters
{
    class SimpleSubstitutionCrypter : SymetricCrypter
    {
        private Dictionary<byte, byte> _map;
        private Dictionary<byte, byte> _mapReverse;
        private bool _initializedState;


        public SimpleSubstitutionCrypter()
        {
        }
        public SimpleSubstitutionCrypter(SimpleSubstitutionKey key)
        {
            this.Key = key;
        }

        protected override void InitCrypto()
        {
            if (this.Key == null)
                throw new  ArgumentNullException(
                    "Simple Substitution Crypter Initialization:" + Environment.NewLine +
                    "Key is null");

            SimpleSubstitutionKey thisKey = (SimpleSubstitutionKey) this.Key;
            _map = new Dictionary<byte, byte>();
            _mapReverse = new Dictionary<byte, byte>();

            foreach (var pair in thisKey.SubstitutionMap)
            {
                _map.Add(pair.Key, pair.Value);
                _mapReverse.Add(pair.Value, pair.Key);    
            }
            _initializedState = true;
        }

        public override byte[] Encrypt(byte[] bytes)
        {
            ExecuteCryptoOperation(bytes, _map);
            return bytes;
        }
        public override byte[] Decrypt(byte[] bytes)
        {
            ExecuteCryptoOperation(bytes, _mapReverse);
            return bytes;
        }

        private void ExecuteCryptoOperation(byte[] bytes, Dictionary<byte,byte> map)
        {
            if (!_initializedState)
                throw new InvalidOperationException("Crypter is not initialized.");

            for (int i = 0; i < bytes.Length; i++)
            {
                bytes[i] = map[bytes[i]];
            }
        }
        protected override bool ValidateKeyType(CryptoKey key)
        {
            return key is SimpleSubstitutionKey;
        }
    }
}
