﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.Crypters.AbstractCrypter;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.Crypters
{
    class RSADecrypter : Decrypter
    {
        protected override bool ValidateKeyType(CryptoKey key)
        {
            return key is RSAPrivateKey;
        }

        protected override void InitCrypto()
        {
            throw new NotImplementedException();
        }

        public override byte[] Decrypt(byte[] bytes)
        {
            throw new NotImplementedException();
        }
    }
}
