﻿namespace ZICrypter.CryptoLib.Crypters.Interfaces
{
    public interface IEncrypter
    {
        byte[] Encrypt(byte[] bytes);
    }
}
