﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZICrypter.CryptoLib.Crypters.Interfaces
{
    public enum StreamCryptoMode
    {
        Enabled,
        Disabled
    }

    interface IStreamCrypto
    {
        StreamCryptoMode Mode { get; set; }
    }
}
