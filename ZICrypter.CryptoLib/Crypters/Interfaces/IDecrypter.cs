﻿namespace ZICrypter.CryptoLib.Crypters.Interfaces
{
    public interface IDecrypter
    {
        byte[] Decrypt(byte[] bytes);
    }
}
