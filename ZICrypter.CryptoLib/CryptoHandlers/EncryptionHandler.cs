﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.Crypters.AbstractCrypter;
using ZICrypter.CryptoLib.Crypters.Interfaces;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.CryptoHandlers
{
    public class EncryptionHandler : CryptoHandler
    {
        protected IEncrypter Encrypter { get; set; }

        public EncryptionHandler(CryptoAlgorithm algorithm, int bufferSize = DefaultBufferSize)
            : base(algorithm, bufferSize)
        {
            Encrypter = CreateEncrypter(algorithm);
        }
        public EncryptionHandler(CryptoAlgorithm algorithm, CryptoKey key, int bufferSize = DefaultBufferSize)
            : this(algorithm, bufferSize)
        {
            Key = key;
        }

        public override CryptoKey Key
        {
            get => ((Crypter)Encrypter).Key;
            set => ((Crypter)Encrypter).Key = value;
        }
        protected override void InvokeCryptOperation(byte[] buffer)
        {
            Encrypter.Encrypt(buffer);
        }
        protected override void ProcessFileHeader(FileStream sourceStream, FileStream destinationStream)
        {
            //todo
        }

        protected override bool HasStreamMode()
        {
            return (Encrypter is IStreamCrypto);
        }
        protected override bool SetStreamMode(StreamCryptoMode mode)
        {
            if (!HasStreamMode()) return false;

            ((IStreamCrypto) Encrypter).Mode = mode;
            return true;
        }
    }
}
