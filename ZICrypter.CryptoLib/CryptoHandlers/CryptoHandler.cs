﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.Crypters;
using ZICrypter.CryptoLib.Crypters.Interfaces;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;
using ZICrypter.CryptoLib.Crypters.AbstractCrypter;


namespace ZICrypter.CryptoLib.CryptoHandlers
{
    public abstract class CryptoHandler
    {
        public const int DefaultBufferSize = 10516480;
        protected int bufferSize;
        #region Static Methods
        protected static IEncrypter CreateEncrypter(CryptoAlgorithm cryptoAlg)
        {
            switch (cryptoAlg)
            {
                case CryptoAlgorithm.SimpleSubstitution:
                    return new SimpleSubstitutionCrypter();
            
                case CryptoAlgorithm.A51:
                    return new A51Crypter();

                case CryptoAlgorithm.RSA:
                        return new RSAEncrypter();
                default:
                    throw new ArgumentOutOfRangeException(nameof(cryptoAlg), cryptoAlg, null);
            }
        }
        protected static IDecrypter CreateDecrypter(CryptoAlgorithm cryptoAlg)
        {
            switch (cryptoAlg)
            {
                case CryptoAlgorithm.SimpleSubstitution:
                    return new SimpleSubstitutionCrypter();

                case CryptoAlgorithm.A51:
                    return new A51Crypter();

                case CryptoAlgorithm.RSA:
                    return new RSADecrypter();
                default:
                    throw new ArgumentOutOfRangeException(nameof(cryptoAlg), cryptoAlg, null);
            }
        }
        #endregion  
        #region Constructors
        protected CryptoHandler(CryptoAlgorithm algorithm, int bufferSize = DefaultBufferSize)
        {
            this.bufferSize = bufferSize;
        }
        protected CryptoHandler(CryptoAlgorithm algorithm, CryptoKey key, int bufferSize = DefaultBufferSize)
            : this(algorithm, bufferSize)
        { }
        #endregion

        public abstract CryptoKey Key { get; set; }

        public virtual void PerformCrypt(byte[] bytes)
        {
            InvokeCryptOperation(bytes);
        }
        public virtual void PerformCrypt(ref string plainText)
        {
            byte[] bytes = Encoding.Default.GetBytes(plainText);
            //Encoding.UTF8.GetBytes(plainText);

            InvokeCryptOperation(bytes);

            plainText = Encoding.Default.GetString(bytes);
            //Encoding.UTF8.GetString(bytes);

        }
        public virtual void PerformCrypt(ref char[] chars)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(chars);

            InvokeCryptOperation(bytes);

            chars = Encoding.UTF8.GetChars(bytes);
        }
        public virtual void PerformCrypt(string filenameSource, string filenameDestination)
        {
            try
            {
                FileInfo sourceFile = new FileInfo(filenameSource);
                FileInfo destinationFile = new FileInfo(filenameDestination);

                FileStream reader = sourceFile.OpenRead();
                FileStream writer = destinationFile.OpenWrite();

                using (reader)
                using (writer)
                {
                    //var header = 
                    ProcessFileHeader(reader, writer);

                    long countFullBuffers  = sourceFile.Length / bufferSize;
                    long partialBufferSize = sourceFile.Length % bufferSize;
                    byte[] buffer;

                    SetStreamMode(StreamCryptoMode.Enabled);

                    if (countFullBuffers != 0)
                    {
                        buffer = new byte[bufferSize];
                        

                        for (int i = 0; i < countFullBuffers; i++)
                        {
                            reader.Read(buffer, 0, bufferSize);
                            InvokeCryptOperation(buffer);
                            writer.Write(buffer, 0, bufferSize);
                        }

                    
                    }

                    buffer = new byte[partialBufferSize];
                    reader.Read(buffer, 0, (int) partialBufferSize);
                    InvokeCryptOperation(buffer);
                    writer.Write(buffer, 0, (int) partialBufferSize);

                    SetStreamMode(StreamCryptoMode.Disabled);
                }
            }
            catch (Exception ex)
            {
                throw new Exception($"Crypting file failed! {ex.Message}", ex); 
            }
            //return header;
        }

        protected abstract void InvokeCryptOperation(byte[] buffer);
        protected abstract void ProcessFileHeader(FileStream sourceStream, FileStream destinationStream);

        protected abstract bool HasStreamMode();
        protected abstract bool SetStreamMode(StreamCryptoMode mode);
        #region Enums
        public enum CryptoAlgorithm
        {
            SimpleSubstitution,
            A51,
            RSA,
            TEA
        }
        public enum CrypterType
        {
            Encrypter,
            Decrypter
        }
        #endregion

    }
}
