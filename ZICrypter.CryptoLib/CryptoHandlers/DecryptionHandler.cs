﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.Crypters.AbstractCrypter;
using ZICrypter.CryptoLib.Crypters.Interfaces;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.CryptoLib.CryptoHandlers
{
    public class DecryptionHandler : CryptoHandler
    {
        protected IDecrypter Decrypter { get; set; }
        
        public DecryptionHandler(CryptoAlgorithm algorithm, int bufferSize = DefaultBufferSize) 
            : base(algorithm, bufferSize)
        {
            Decrypter = CreateDecrypter(algorithm);
        }
        public DecryptionHandler(CryptoAlgorithm algorithm, CryptoKey key, int bufferSize = DefaultBufferSize) 
            : this(algorithm, bufferSize)
        {
            Key = key;
        }

        public override CryptoKey Key
        {
            get => ((Crypter)Decrypter).Key;
            set => ((Crypter)Decrypter).Key = value;
        }
        protected override void InvokeCryptOperation(byte[] buffer)
        {
            Decrypter.Decrypt(buffer);
        }
        protected override void ProcessFileHeader(FileStream sourceStream, FileStream destinationStream)
        {
            //todo
        }

        protected override bool HasStreamMode()
        {
            return (Decrypter is IStreamCrypto);
        }
        protected override bool SetStreamMode(StreamCryptoMode mode)
        {
            if (!HasStreamMode()) return false;

            ((IStreamCrypto)Decrypter).Mode = mode;
            return true;
        }
    }
}
