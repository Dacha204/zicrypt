﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.Communicator;
using ZICrypter.Communicator.EventArg;
using ZICrypter.Communicator.Model;
using ZICrypter.CryptoLib.Crypters.Interfaces;
using ZICrypter.CryptoLib.CryptoHandlers;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.FSW;

namespace DebugConsole
{
    class Program
    {
        private static string SourceDir = "F:\\ZICryptDir";
        private static string DestDir = "F:\\ZICryptDir\\Encrypted";

        static void Main(string[] args)
        {

            //Communicator();

            
            
            
            FSW();
            //A51DemoFiles();
            //A51DemoText();
            //SimpleSubstitutionDemoText();
            //SimpleSubstitutionDemoFiles();
        }


        #region COMMUNICATIONS
        static void Communicator()
        {
            Console.WriteLine("Enter username: ");
            string username = Console.ReadLine();
            bool loop = true;

            using (ZICommunicator communicator = new ZICommunicator(username)) //bitno
            {
                communicator.OnNewMessage += OnNewMessage;      //bitno
                communicator.OnNewOnlineClient += OnNewOnline;  //bitno

                while (loop)
                {
                 
                    Console.Write("Enter message: ");
                    string message = Console.ReadLine();

                    Console.WriteLine("=== Online clients ===");
                    foreach (Client cl in communicator.Clients)
                    {
                        Console.WriteLine($"{cl.Id}. {cl.Username}");
                    }
                    Console.WriteLine("======================");
                    Console.WriteLine("Choose receivers (separated by space)");

                    string input = Console.ReadLine();
                    string[] receivers = input?.Split(' ');
                    List<Client> clients = new List<Client>();
                    foreach (string recv in receivers)
                    {
                        int.TryParse(recv, out int recvId);
                        Client c = communicator.Clients.Find(x => x.Id == recvId);
                        if (c != null)
                            clients.Add(c);
                    }

                    if (clients.Count != 0)
                        communicator.SendMessage(message, clients); //bitno
                    
                    Console.WriteLine("Send more? yes/no");
                    string answer = Console.ReadLine()?.ToLower();
                    loop = answer == "yes";
                }
            }
            
        }

        //Event listener: New message
        static void OnNewMessage(object sender, MessageReceivedEventArgs e)
        {
            Console.WriteLine("\t========== OnNewMessage Handler =========");
            Console.WriteLine("\tMessageText: " + e.Message.MessageText);
            Console.WriteLine("\t=========================================");
        }

        //Event listener: New online user
        static void OnNewOnline(object sender, NewOnlineEventArgs e)
        {
            Console.WriteLine("\t=========== New Online Handler ==========");
            Console.WriteLine("\t=== Online clients ===");
            foreach (Client cl in e.OnlineClients)
            {
                Console.WriteLine($"\t{cl.Id}. {cl.Username}");
            }
            Console.WriteLine("\t=========================================");
        }
        
        #endregion


        // =========================

        //Finished demos
        [PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
        static void FSW()
        {
            FSWCrytper fsw = new FSWCrytper(SourceDir, DestDir);
            fsw.OnFSWEvent += FSW_OnFSWEvent;

            fsw.StartRestored();

            Console.WriteLine("Running...");
            Console.WriteLine("Press anykey to Decrypt all files");
            Console.Read();
            fsw.DecryptAll();

            fsw.StopAndSave();
        }

        private static void FSW_OnFSWEvent(object sender, ZICrypter.FSW.EventArgs.FSWEventArg e)
        {
            Console.WriteLine($"{e.Message}");
        }

        static void A51DemoFiles()
        {
            EncryptionHandler encrypter = new EncryptionHandler(CryptoHandler.CryptoAlgorithm.A51);
            DecryptionHandler decrypter = new DecryptionHandler(CryptoHandler.CryptoAlgorithm.A51);

            A51Key key = new A51Key();
            key.RegistryX.Set(0, true); key.RegistryX.Set(5, true); key.RegistryX.Set(7, true);
            key.RegistryX.Set(8, true); key.RegistryX.Set(12, true); key.RegistryX.Set(13, true);
            key.RegistryX.Set(16, true); key.RegistryX.Set(18, true);

            key.RegistryY.Set(2, true); key.RegistryY.Set(6, true); key.RegistryY.Set(8, true);
            key.RegistryY.Set(10, true); key.RegistryY.Set(13, true); key.RegistryY.Set(17, true);
            key.RegistryY.Set(20, true); key.RegistryY.Set(21, true);

            key.RegistryZ.Set(4, true); key.RegistryZ.Set(5, true); key.RegistryZ.Set(7, true);
            key.RegistryZ.Set(9, true); key.RegistryZ.Set(13, true); key.RegistryZ.Set(14, true);
            key.RegistryZ.Set(15, true); key.RegistryZ.Set(19, true); key.RegistryZ.Set(22, true);

            encrypter.Key = key;
            decrypter.Key = key;

            Console.WriteLine("Crypto started.");

            Stopwatch watch = new Stopwatch();
            watch.Start();
            encrypter.PerformCrypt("ImageOriginal.jpg", "A51.jpg.encrypted");
            watch.Stop();

            TimeSpan encryptTime = watch.Elapsed;
            Console.WriteLine($"Encrypt: {encryptTime.TotalMilliseconds}");
            
            watch.Reset();
            watch.Start();
            decrypter.PerformCrypt("A51.jpg.encrypted", "A51Decrypted.jpg");
            watch.Stop();

            TimeSpan decryptTime = watch.Elapsed;
            Console.WriteLine($"DecryptTime: {decryptTime.TotalMilliseconds}");

            //////text file
            Console.WriteLine("Crypto started.");

            watch = new Stopwatch();
            watch.Start();
            encrypter.PerformCrypt("PlainText.txt", "A51.txt.encrypted");
            watch.Stop();

            encryptTime = watch.Elapsed;
            Console.WriteLine($"Encrypt: {encryptTime.TotalMilliseconds}");

            watch.Reset();
            watch.Start();
            decrypter.PerformCrypt("A51.txt.encrypted", "A51Decrypted.txt");
            watch.Stop();

            decryptTime = watch.Elapsed;
            Console.WriteLine($"DecryptTime: {decryptTime.TotalMilliseconds}");

        }
        static void A51DemoText()
        {
            EncryptionHandler encrypter = new EncryptionHandler(CryptoHandler.CryptoAlgorithm.A51);
            DecryptionHandler decrypter = new DecryptionHandler(CryptoHandler.CryptoAlgorithm.A51);

            A51Key key = new A51Key();
            key.RegistryX.Set(0, true);  key.RegistryX.Set(5, true);  key.RegistryX.Set(7, true);
            key.RegistryX.Set(8, true);  key.RegistryX.Set(12, true); key.RegistryX.Set(13, true);
            key.RegistryX.Set(16, true); key.RegistryX.Set(18, true);

            key.RegistryY.Set(2, true);  key.RegistryY.Set(6, true);  key.RegistryY.Set(8, true);
            key.RegistryY.Set(10, true); key.RegistryY.Set(13, true); key.RegistryY.Set(17, true);
            key.RegistryY.Set(20, true); key.RegistryY.Set(21, true);

            key.RegistryZ.Set(4, true); key.RegistryZ.Set(5, true); key.RegistryZ.Set(7, true);
            key.RegistryZ.Set(9, true); key.RegistryZ.Set(13, true); key.RegistryZ.Set(14, true);
            key.RegistryZ.Set(15, true); key.RegistryZ.Set(19, true); key.RegistryZ.Set(22, true);

            encrypter.Key = key;
            decrypter.Key = key;

            string Text = "Hello";
            Console.WriteLine($"Original: {Text}");

            encrypter.PerformCrypt(ref Text);
            Console.WriteLine($"Encrypted: {Text}");

            decrypter.PerformCrypt(ref Text);
            Console.WriteLine($"Decrypted: {Text}");
        }
        static void SaveKeyTest()
        {
            RSAPublicKey key = new RSAPublicKey();

            key.E = UInt64.MaxValue;
            key.N = UInt64.MinValue;

            key.SaveToFile("RSAKey.xml");
        }
        static void SimpleSubstitutionDemoFiles()
        {
            EncryptionHandler encrypter = new EncryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution);
            DecryptionHandler decrypter = new DecryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution);

            SimpleSubstitutionKey key = new SimpleSubstitutionKey();
            for (int i = 0; i < 256; i++)
            {
                key.SubstitutionMap.Add((byte)i, (byte)~i);
            }

            encrypter.Key = key;
            decrypter.Key = key;


            Stopwatch watch = new Stopwatch();
            watch.Start();
            encrypter.PerformCrypt("Original.pdf", "SSub.pdf.encrypted");
            watch.Stop();

            TimeSpan encryptTime = watch.Elapsed;

            watch.Reset();
            watch.Start();
            decrypter.PerformCrypt("SSub.pdf.encrypted", "SSubDecrypted.pdf");
            watch.Stop();

            TimeSpan decryptTime = watch.Elapsed;

            Console.WriteLine($"Encrypt: {encryptTime.TotalMilliseconds}");
            Console.WriteLine($"DecryptTime: {decryptTime.TotalMilliseconds}");
        }
        static void SimpleSubstitutionDemoText()
        {
            EncryptionHandler encrypter = new EncryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution);
            DecryptionHandler decrypter = new DecryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution);

            SimpleSubstitutionKey key = new SimpleSubstitutionKey();

            key.SubstitutionMap.Add((byte)'A', (byte)'0');
            key.SubstitutionMap.Add((byte)'B', (byte)'1');
            key.SubstitutionMap.Add((byte)'C', (byte)'2');
            key.SubstitutionMap.Add((byte)'D', (byte)'3');
            key.SubstitutionMap.Add((byte)'E', (byte)'4');
            key.SubstitutionMap.Add((byte)'F', (byte)'5');
            key.SubstitutionMap.Add((byte)'G', (byte)'6');


            encrypter.Key = key;
            decrypter.Key = key;

            string text = "ABADAFA";

            encrypter.PerformCrypt(ref text);

            decrypter.PerformCrypt(ref text);
        }
        static void BitShiftingTest()
        {
            int size = 10;
            BitArray bitarray = new BitArray(size);
            bool[] boolarray = new bool[size];
            Random rand = new Random(DateTime.Now.Millisecond);
            bool last;

            Console.WriteLine("Shifting 5 000 000 times...");

            for (int i = 0; i < size; i++)
            {
                int r = rand.Next(2);
                bool res = r == 1;
                bitarray[i] = res;
                boolarray[i] = res;
            }

            Stopwatch watch = new Stopwatch();

            watch.Restart();
            for (int j = 0; j < 5000000; j++)
            {
                last = bitarray[size - 1];
                for (int i = size - 1; i > 0; i--)
                {
                    bitarray[i] = bitarray[i - 1];
                }
                bitarray[0] = last;
            }
            watch.Stop();
            Console.WriteLine("BitArray: " + watch.ElapsedMilliseconds + "ms");


           watch.Restart();
            for (int j = 0; j < 5000000; j++)
            {
                last = boolarray[size - 1];
                for (int i = size - 1; i > 0; i--)
                {
                    boolarray[i] = boolarray[i - 1];
                }
                boolarray[0] = last;
            }
            watch.Stop();
            Console.WriteLine("BoolArray: " + watch.ElapsedMilliseconds + "ms");

            watch.Restart();
            for (int j = 0; j < 5000000; j++)
            {
                last = bitarray[size - 1];
                for (int i = size - 1; i > 0; i--)
                {
                    bitarray[i] = bitarray[i - 1];
                }
                bitarray[0] = last;
            }
            watch.Stop();
            Console.WriteLine("BitArray: " + watch.ElapsedMilliseconds + "ms");

            watch.Restart();
            for (int j = 0; j < 5000000; j++)
            {
                last = boolarray[size - 1];
                for (int i = size - 1; i > 0; i--)
                {
                    boolarray[i] = boolarray[i - 1];
                }
                boolarray[0] = last;
            }
            watch.Stop();
            Console.WriteLine("BoolArray: " + watch.ElapsedMilliseconds + "ms");
        }
    }
}
