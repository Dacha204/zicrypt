﻿namespace ZICrypter
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusMessage = new System.Windows.Forms.ToolStripStatusLabel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.manualToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabA1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonDecryptSS = new System.Windows.Forms.Button();
            this.textBoxPlainTextSS = new System.Windows.Forms.TextBox();
            this.textBoxEncryptedTextSS = new System.Windows.Forms.TextBox();
            this.buttonEncryptSS = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBoxSettings = new System.Windows.Forms.GroupBox();
            this.dataGridViewSSKey = new System.Windows.Forms.DataGridView();
            this.buttonLoadKeySS = new System.Windows.Forms.Button();
            this.buttonSaveKeyFileSS = new System.Windows.Forms.Button();
            this.buttonLoadKeyFileSS = new System.Windows.Forms.Button();
            this.tabA2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonDecryptA51 = new System.Windows.Forms.Button();
            this.textBoxPlainTextA51 = new System.Windows.Forms.TextBox();
            this.textBoxEncryptedTextA51 = new System.Windows.Forms.TextBox();
            this.buttonEncryptA51 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBoxShiftGenZ = new System.Windows.Forms.TextBox();
            this.textBoxShiftGenY = new System.Windows.Forms.TextBox();
            this.textBoxShiftGenX = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.comboBoxMajorityZ = new System.Windows.Forms.ComboBox();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBoxMajorityY = new System.Windows.Forms.ComboBox();
            this.label20 = new System.Windows.Forms.Label();
            this.comboBoxMajorityX = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.buttonLoadKeyA51 = new System.Windows.Forms.Button();
            this.buttonSaveKeyFileA51 = new System.Windows.Forms.Button();
            this.buttonLoadKeyFileA51 = new System.Windows.Forms.Button();
            this.textBoxKeyA51 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.tabA3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonDecryptTEA = new System.Windows.Forms.Button();
            this.textBoxPlainTextTEA = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.buttonEncryptTEA = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.buttonLoadKeyTEA = new System.Windows.Forms.Button();
            this.buttonSaveKeyFileTEA = new System.Windows.Forms.Button();
            this.buttonLoadKeyFileTEA = new System.Windows.Forms.Button();
            this.textBoxKeyTEA = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tabA4 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonDecryptRSA = new System.Windows.Forms.Button();
            this.textBoxPlainTextRSA = new System.Windows.Forms.TextBox();
            this.textBoxEncryptedTextRSA = new System.Windows.Forms.TextBox();
            this.buttonEncryptRSA = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.buttonLoadKeyRSAPublic = new System.Windows.Forms.Button();
            this.buttonSaveKeyFileRSAPublic = new System.Windows.Forms.Button();
            this.buttonLoadKeyFilePublic = new System.Windows.Forms.Button();
            this.textBoxKeyRSAPublic = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.buttonLoadKeyRSAPrivate = new System.Windows.Forms.Button();
            this.buttonSaveKeyFileRSAPrivate = new System.Windows.Forms.Button();
            this.buttonLoadKeyFileRSAPrivate = new System.Windows.Forms.Button();
            this.textBoxKeyRSAPrivate = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.tabFSW = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.textBoxSourceDir = new System.Windows.Forms.TextBox();
            this.btnSourceDirSelect = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textBoxDestDir = new System.Windows.Forms.TextBox();
            this.btnDestinationDirSelect = new System.Windows.Forms.Button();
            this.buttonStopFSW = new System.Windows.Forms.Button();
            this.buttonStartFSW = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.buttonDecryptAll = new System.Windows.Forms.Button();
            this.textBoxConsoleFSW = new System.Windows.Forms.TextBox();
            this.tabSimulation = new System.Windows.Forms.TabPage();
            this.tabMessenger = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonNickname = new System.Windows.Forms.Button();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.textBoxReceivedMessages = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.panelSendMessage = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.buttonSend = new System.Windows.Forms.Button();
            this.textBoxMessage = new System.Windows.Forms.TextBox();
            this.lable19 = new System.Windows.Forms.Label();
            this.openFileDialogKey = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialogKey = new System.Windows.Forms.SaveFileDialog();
            this.TrayIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.listBoxAvailableUsers = new System.Windows.Forms.ListBox();
            this.statusStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tabA1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBoxSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSSKey)).BeginInit();
            this.tabA2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tabA3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabA4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabFSW.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tabMessenger.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panelSendMessage.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusMessage});
            this.statusStrip1.Location = new System.Drawing.Point(0, 698);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1025, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusMessage
            // 
            this.toolStripStatusMessage.Name = "toolStripStatusMessage";
            this.toolStripStatusMessage.Size = new System.Drawing.Size(39, 17);
            this.toolStripStatusMessage.Text = "Ready";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1025, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.manualToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // manualToolStripMenuItem
            // 
            this.manualToolStripMenuItem.Name = "manualToolStripMenuItem";
            this.manualToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.manualToolStripMenuItem.Text = "Manual";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabA1);
            this.tabControl.Controls.Add(this.tabA2);
            this.tabControl.Controls.Add(this.tabA3);
            this.tabControl.Controls.Add(this.tabA4);
            this.tabControl.Controls.Add(this.tabFSW);
            this.tabControl.Controls.Add(this.tabSimulation);
            this.tabControl.Controls.Add(this.tabMessenger);
            this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl.Location = new System.Drawing.Point(0, 24);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1025, 674);
            this.tabControl.TabIndex = 4;
            // 
            // tabA1
            // 
            this.tabA1.Controls.Add(this.tableLayoutPanel1);
            this.tabA1.Location = new System.Drawing.Point(4, 22);
            this.tabA1.Name = "tabA1";
            this.tabA1.Size = new System.Drawing.Size(1017, 648);
            this.tabA1.TabIndex = 0;
            this.tabA1.Text = "Simple Substitution";
            this.tabA1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.buttonDecryptSS, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.textBoxPlainTextSS, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.textBoxEncryptedTextSS, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.buttonEncryptSS, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.groupBoxSettings, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1017, 648);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // buttonDecryptSS
            // 
            this.buttonDecryptSS.Enabled = false;
            this.buttonDecryptSS.Location = new System.Drawing.Point(511, 428);
            this.buttonDecryptSS.Name = "buttonDecryptSS";
            this.buttonDecryptSS.Size = new System.Drawing.Size(180, 37);
            this.buttonDecryptSS.TabIndex = 3;
            this.buttonDecryptSS.Text = "Decrypt";
            this.buttonDecryptSS.UseVisualStyleBackColor = true;
            this.buttonDecryptSS.Click += new System.EventHandler(this.buttonDecryptSS_Click);
            // 
            // textBoxPlainTextSS
            // 
            this.textBoxPlainTextSS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPlainTextSS.Enabled = false;
            this.textBoxPlainTextSS.Location = new System.Drawing.Point(3, 27);
            this.textBoxPlainTextSS.Multiline = true;
            this.textBoxPlainTextSS.Name = "textBoxPlainTextSS";
            this.textBoxPlainTextSS.Size = new System.Drawing.Size(502, 395);
            this.textBoxPlainTextSS.TabIndex = 0;
            // 
            // textBoxEncryptedTextSS
            // 
            this.textBoxEncryptedTextSS.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxEncryptedTextSS.Enabled = false;
            this.textBoxEncryptedTextSS.Location = new System.Drawing.Point(511, 27);
            this.textBoxEncryptedTextSS.Multiline = true;
            this.textBoxEncryptedTextSS.Name = "textBoxEncryptedTextSS";
            this.textBoxEncryptedTextSS.Size = new System.Drawing.Size(503, 395);
            this.textBoxEncryptedTextSS.TabIndex = 1;
            // 
            // buttonEncryptSS
            // 
            this.buttonEncryptSS.Enabled = false;
            this.buttonEncryptSS.Location = new System.Drawing.Point(3, 428);
            this.buttonEncryptSS.Name = "buttonEncryptSS";
            this.buttonEncryptSS.Size = new System.Drawing.Size(180, 37);
            this.buttonEncryptSS.TabIndex = 2;
            this.buttonEncryptSS.Text = "Encrypt";
            this.buttonEncryptSS.UseVisualStyleBackColor = true;
            this.buttonEncryptSS.Click += new System.EventHandler(this.buttonEncryptSS_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(502, 24);
            this.label1.TabIndex = 3;
            this.label1.Text = "Plain Text";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(511, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(503, 24);
            this.label2.TabIndex = 4;
            this.label2.Text = "Encrypted";
            // 
            // groupBoxSettings
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.groupBoxSettings, 2);
            this.groupBoxSettings.Controls.Add(this.dataGridViewSSKey);
            this.groupBoxSettings.Controls.Add(this.buttonLoadKeySS);
            this.groupBoxSettings.Controls.Add(this.buttonSaveKeyFileSS);
            this.groupBoxSettings.Controls.Add(this.buttonLoadKeyFileSS);
            this.groupBoxSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxSettings.Location = new System.Drawing.Point(3, 473);
            this.groupBoxSettings.Name = "groupBoxSettings";
            this.groupBoxSettings.Size = new System.Drawing.Size(1011, 172);
            this.groupBoxSettings.TabIndex = 5;
            this.groupBoxSettings.TabStop = false;
            this.groupBoxSettings.Text = " Settings ";
            // 
            // dataGridViewSSKey
            // 
            this.dataGridViewSSKey.AllowUserToAddRows = false;
            this.dataGridViewSSKey.AllowUserToDeleteRows = false;
            this.dataGridViewSSKey.AllowUserToResizeColumns = false;
            this.dataGridViewSSKey.AllowUserToResizeRows = false;
            this.dataGridViewSSKey.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridViewSSKey.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewSSKey.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dataGridViewSSKey.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle10.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle10.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewSSKey.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewSSKey.Location = new System.Drawing.Point(6, 16);
            this.dataGridViewSSKey.Name = "dataGridViewSSKey";
            this.dataGridViewSSKey.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dataGridViewSSKey.RowHeadersVisible = false;
            this.dataGridViewSSKey.Size = new System.Drawing.Size(896, 150);
            this.dataGridViewSSKey.TabIndex = 5;
            // 
            // buttonLoadKeySS
            // 
            this.buttonLoadKeySS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoadKeySS.Location = new System.Drawing.Point(908, 16);
            this.buttonLoadKeySS.Name = "buttonLoadKeySS";
            this.buttonLoadKeySS.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeySS.TabIndex = 4;
            this.buttonLoadKeySS.Text = "Set key";
            this.buttonLoadKeySS.UseVisualStyleBackColor = true;
            this.buttonLoadKeySS.Click += new System.EventHandler(this.buttonLoadKeySS_Click);
            // 
            // buttonSaveKeyFileSS
            // 
            this.buttonSaveKeyFileSS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveKeyFileSS.Enabled = false;
            this.buttonSaveKeyFileSS.Location = new System.Drawing.Point(908, 90);
            this.buttonSaveKeyFileSS.Name = "buttonSaveKeyFileSS";
            this.buttonSaveKeyFileSS.Size = new System.Drawing.Size(97, 31);
            this.buttonSaveKeyFileSS.TabIndex = 3;
            this.buttonSaveKeyFileSS.Text = "Save to file...";
            this.buttonSaveKeyFileSS.UseVisualStyleBackColor = true;
            this.buttonSaveKeyFileSS.Click += new System.EventHandler(this.buttonSaveKeyFileSS_Click);
            // 
            // buttonLoadKeyFileSS
            // 
            this.buttonLoadKeyFileSS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoadKeyFileSS.Location = new System.Drawing.Point(908, 53);
            this.buttonLoadKeyFileSS.Name = "buttonLoadKeyFileSS";
            this.buttonLoadKeyFileSS.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeyFileSS.TabIndex = 2;
            this.buttonLoadKeyFileSS.Text = "Load from file...";
            this.buttonLoadKeyFileSS.UseVisualStyleBackColor = true;
            this.buttonLoadKeyFileSS.Click += new System.EventHandler(this.buttonLoadKeyFileSS_Click);
            // 
            // tabA2
            // 
            this.tabA2.Controls.Add(this.tableLayoutPanel4);
            this.tabA2.Location = new System.Drawing.Point(4, 22);
            this.tabA2.Name = "tabA2";
            this.tabA2.Size = new System.Drawing.Size(1017, 648);
            this.tabA2.TabIndex = 3;
            this.tabA2.Text = "A5/1";
            this.tabA2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.buttonDecryptA51, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.textBoxPlainTextA51, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.textBoxEncryptedTextA51, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.buttonEncryptA51, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label10, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.groupBox3, 0, 3);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1017, 648);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // buttonDecryptA51
            // 
            this.buttonDecryptA51.Enabled = false;
            this.buttonDecryptA51.Location = new System.Drawing.Point(511, 428);
            this.buttonDecryptA51.Name = "buttonDecryptA51";
            this.buttonDecryptA51.Size = new System.Drawing.Size(180, 37);
            this.buttonDecryptA51.TabIndex = 3;
            this.buttonDecryptA51.Text = "Decrypt";
            this.buttonDecryptA51.UseVisualStyleBackColor = true;
            this.buttonDecryptA51.Click += new System.EventHandler(this.buttonDecryptA51_Click);
            // 
            // textBoxPlainTextA51
            // 
            this.textBoxPlainTextA51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPlainTextA51.Enabled = false;
            this.textBoxPlainTextA51.Location = new System.Drawing.Point(3, 27);
            this.textBoxPlainTextA51.Multiline = true;
            this.textBoxPlainTextA51.Name = "textBoxPlainTextA51";
            this.textBoxPlainTextA51.Size = new System.Drawing.Size(502, 395);
            this.textBoxPlainTextA51.TabIndex = 0;
            // 
            // textBoxEncryptedTextA51
            // 
            this.textBoxEncryptedTextA51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxEncryptedTextA51.Enabled = false;
            this.textBoxEncryptedTextA51.Location = new System.Drawing.Point(511, 27);
            this.textBoxEncryptedTextA51.Multiline = true;
            this.textBoxEncryptedTextA51.Name = "textBoxEncryptedTextA51";
            this.textBoxEncryptedTextA51.Size = new System.Drawing.Size(503, 395);
            this.textBoxEncryptedTextA51.TabIndex = 1;
            // 
            // buttonEncryptA51
            // 
            this.buttonEncryptA51.Enabled = false;
            this.buttonEncryptA51.Location = new System.Drawing.Point(3, 428);
            this.buttonEncryptA51.Name = "buttonEncryptA51";
            this.buttonEncryptA51.Size = new System.Drawing.Size(180, 37);
            this.buttonEncryptA51.TabIndex = 2;
            this.buttonEncryptA51.Text = "Encrypt";
            this.buttonEncryptA51.UseVisualStyleBackColor = true;
            this.buttonEncryptA51.Click += new System.EventHandler(this.buttonEncryptA51_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Top;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(502, 24);
            this.label9.TabIndex = 3;
            this.label9.Text = "Plain Text";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Top;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(511, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(503, 24);
            this.label10.TabIndex = 4;
            this.label10.Text = "Encrypted";
            // 
            // groupBox3
            // 
            this.tableLayoutPanel4.SetColumnSpan(this.groupBox3, 2);
            this.groupBox3.Controls.Add(this.textBoxShiftGenZ);
            this.groupBox3.Controls.Add(this.textBoxShiftGenY);
            this.groupBox3.Controls.Add(this.textBoxShiftGenX);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.label24);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.label22);
            this.groupBox3.Controls.Add(this.comboBoxMajorityZ);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.comboBoxMajorityY);
            this.groupBox3.Controls.Add(this.label20);
            this.groupBox3.Controls.Add(this.comboBoxMajorityX);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.buttonLoadKeyA51);
            this.groupBox3.Controls.Add(this.buttonSaveKeyFileA51);
            this.groupBox3.Controls.Add(this.buttonLoadKeyFileA51);
            this.groupBox3.Controls.Add(this.textBoxKeyA51);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(3, 473);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1011, 172);
            this.groupBox3.TabIndex = 5;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = " Settings ";
            // 
            // textBoxShiftGenZ
            // 
            this.textBoxShiftGenZ.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxShiftGenZ.Location = new System.Drawing.Point(243, 127);
            this.textBoxShiftGenZ.Name = "textBoxShiftGenZ";
            this.textBoxShiftGenZ.Size = new System.Drawing.Size(659, 20);
            this.textBoxShiftGenZ.TabIndex = 19;
            this.textBoxShiftGenZ.Text = "7,20,21,22";
            // 
            // textBoxShiftGenY
            // 
            this.textBoxShiftGenY.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxShiftGenY.Location = new System.Drawing.Point(243, 101);
            this.textBoxShiftGenY.Name = "textBoxShiftGenY";
            this.textBoxShiftGenY.Size = new System.Drawing.Size(659, 20);
            this.textBoxShiftGenY.TabIndex = 18;
            this.textBoxShiftGenY.Text = "20,21";
            // 
            // textBoxShiftGenX
            // 
            this.textBoxShiftGenX.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxShiftGenX.Location = new System.Drawing.Point(243, 73);
            this.textBoxShiftGenX.Name = "textBoxShiftGenX";
            this.textBoxShiftGenX.Size = new System.Drawing.Size(659, 20);
            this.textBoxShiftGenX.TabIndex = 17;
            this.textBoxShiftGenX.Text = "13,16,17,18";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(150, 130);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(87, 13);
            this.label25.TabIndex = 16;
            this.label25.Text = "ShiftGeneratorsZ";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(150, 103);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(87, 13);
            this.label24.TabIndex = 15;
            this.label24.Text = "ShiftGeneratorsY";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(150, 76);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(87, 13);
            this.label23.TabIndex = 14;
            this.label23.Text = "ShiftGeneratorsX";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 130);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(52, 13);
            this.label22.TabIndex = 13;
            this.label22.Text = "RegistryZ";
            // 
            // comboBoxMajorityZ
            // 
            this.comboBoxMajorityZ.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMajorityZ.FormattingEnabled = true;
            this.comboBoxMajorityZ.Location = new System.Drawing.Point(64, 127);
            this.comboBoxMajorityZ.Name = "comboBoxMajorityZ";
            this.comboBoxMajorityZ.Size = new System.Drawing.Size(65, 21);
            this.comboBoxMajorityZ.TabIndex = 12;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(6, 103);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(52, 13);
            this.label21.TabIndex = 11;
            this.label21.Text = "RegistryY";
            // 
            // comboBoxMajorityY
            // 
            this.comboBoxMajorityY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMajorityY.FormattingEnabled = true;
            this.comboBoxMajorityY.Location = new System.Drawing.Point(64, 100);
            this.comboBoxMajorityY.Name = "comboBoxMajorityY";
            this.comboBoxMajorityY.Size = new System.Drawing.Size(65, 21);
            this.comboBoxMajorityY.TabIndex = 10;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 76);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(52, 13);
            this.label20.TabIndex = 9;
            this.label20.Text = "RegistryX";
            // 
            // comboBoxMajorityX
            // 
            this.comboBoxMajorityX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMajorityX.FormattingEnabled = true;
            this.comboBoxMajorityX.Location = new System.Drawing.Point(64, 73);
            this.comboBoxMajorityX.Name = "comboBoxMajorityX";
            this.comboBoxMajorityX.Size = new System.Drawing.Size(65, 21);
            this.comboBoxMajorityX.TabIndex = 6;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "MajorityBits ";
            // 
            // buttonLoadKeyA51
            // 
            this.buttonLoadKeyA51.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoadKeyA51.Location = new System.Drawing.Point(908, 32);
            this.buttonLoadKeyA51.Name = "buttonLoadKeyA51";
            this.buttonLoadKeyA51.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeyA51.TabIndex = 4;
            this.buttonLoadKeyA51.Text = "Set key";
            this.buttonLoadKeyA51.UseVisualStyleBackColor = true;
            this.buttonLoadKeyA51.Click += new System.EventHandler(this.buttonLoadKeyA51_Click);
            // 
            // buttonSaveKeyFileA51
            // 
            this.buttonSaveKeyFileA51.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveKeyFileA51.Enabled = false;
            this.buttonSaveKeyFileA51.Location = new System.Drawing.Point(908, 106);
            this.buttonSaveKeyFileA51.Name = "buttonSaveKeyFileA51";
            this.buttonSaveKeyFileA51.Size = new System.Drawing.Size(97, 31);
            this.buttonSaveKeyFileA51.TabIndex = 3;
            this.buttonSaveKeyFileA51.Text = "Save to file...";
            this.buttonSaveKeyFileA51.UseVisualStyleBackColor = true;
            this.buttonSaveKeyFileA51.Click += new System.EventHandler(this.buttonSaveKeyFileA51_Click);
            // 
            // buttonLoadKeyFileA51
            // 
            this.buttonLoadKeyFileA51.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonLoadKeyFileA51.Location = new System.Drawing.Point(908, 69);
            this.buttonLoadKeyFileA51.Name = "buttonLoadKeyFileA51";
            this.buttonLoadKeyFileA51.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeyFileA51.TabIndex = 2;
            this.buttonLoadKeyFileA51.Text = "Load from file...";
            this.buttonLoadKeyFileA51.UseVisualStyleBackColor = true;
            this.buttonLoadKeyFileA51.Click += new System.EventHandler(this.buttonLoadKeyFileA51_Click);
            // 
            // textBoxKeyA51
            // 
            this.textBoxKeyA51.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxKeyA51.Location = new System.Drawing.Point(6, 32);
            this.textBoxKeyA51.Name = "textBoxKeyA51";
            this.textBoxKeyA51.Size = new System.Drawing.Size(896, 20);
            this.textBoxKeyA51.TabIndex = 1;
            this.textBoxKeyA51.Text = "9309800112817540126";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "64bit Registry Key";
            // 
            // tabA3
            // 
            this.tabA3.Controls.Add(this.tableLayoutPanel5);
            this.tabA3.Location = new System.Drawing.Point(4, 22);
            this.tabA3.Name = "tabA3";
            this.tabA3.Size = new System.Drawing.Size(1017, 648);
            this.tabA3.TabIndex = 4;
            this.tabA3.Text = "TEA/XTEA";
            this.tabA3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.buttonDecryptTEA, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.textBoxPlainTextTEA, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.textBox11, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.buttonEncryptTEA, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.label13, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.groupBox4, 0, 3);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1017, 648);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // buttonDecryptTEA
            // 
            this.buttonDecryptTEA.Location = new System.Drawing.Point(511, 428);
            this.buttonDecryptTEA.Name = "buttonDecryptTEA";
            this.buttonDecryptTEA.Size = new System.Drawing.Size(180, 37);
            this.buttonDecryptTEA.TabIndex = 3;
            this.buttonDecryptTEA.Text = "Decrypt";
            this.buttonDecryptTEA.UseVisualStyleBackColor = true;
            // 
            // textBoxPlainTextTEA
            // 
            this.textBoxPlainTextTEA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPlainTextTEA.Location = new System.Drawing.Point(3, 27);
            this.textBoxPlainTextTEA.Multiline = true;
            this.textBoxPlainTextTEA.Name = "textBoxPlainTextTEA";
            this.textBoxPlainTextTEA.Size = new System.Drawing.Size(502, 395);
            this.textBoxPlainTextTEA.TabIndex = 0;
            // 
            // textBox11
            // 
            this.textBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox11.Location = new System.Drawing.Point(511, 27);
            this.textBox11.Multiline = true;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(503, 395);
            this.textBox11.TabIndex = 1;
            // 
            // buttonEncryptTEA
            // 
            this.buttonEncryptTEA.Location = new System.Drawing.Point(3, 428);
            this.buttonEncryptTEA.Name = "buttonEncryptTEA";
            this.buttonEncryptTEA.Size = new System.Drawing.Size(180, 37);
            this.buttonEncryptTEA.TabIndex = 2;
            this.buttonEncryptTEA.Text = "Encrypt";
            this.buttonEncryptTEA.UseVisualStyleBackColor = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Top;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(502, 24);
            this.label12.TabIndex = 3;
            this.label12.Text = "Plain Text";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Top;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(511, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(503, 24);
            this.label13.TabIndex = 4;
            this.label13.Text = "Encrypted";
            // 
            // groupBox4
            // 
            this.tableLayoutPanel5.SetColumnSpan(this.groupBox4, 2);
            this.groupBox4.Controls.Add(this.buttonLoadKeyTEA);
            this.groupBox4.Controls.Add(this.buttonSaveKeyFileTEA);
            this.groupBox4.Controls.Add(this.buttonLoadKeyFileTEA);
            this.groupBox4.Controls.Add(this.textBoxKeyTEA);
            this.groupBox4.Controls.Add(this.label14);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Location = new System.Drawing.Point(3, 473);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1011, 172);
            this.groupBox4.TabIndex = 5;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " Settings ";
            // 
            // buttonLoadKeyTEA
            // 
            this.buttonLoadKeyTEA.Location = new System.Drawing.Point(6, 58);
            this.buttonLoadKeyTEA.Name = "buttonLoadKeyTEA";
            this.buttonLoadKeyTEA.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeyTEA.TabIndex = 4;
            this.buttonLoadKeyTEA.Text = "Load key";
            this.buttonLoadKeyTEA.UseVisualStyleBackColor = true;
            // 
            // buttonSaveKeyFileTEA
            // 
            this.buttonSaveKeyFileTEA.Location = new System.Drawing.Point(212, 58);
            this.buttonSaveKeyFileTEA.Name = "buttonSaveKeyFileTEA";
            this.buttonSaveKeyFileTEA.Size = new System.Drawing.Size(97, 31);
            this.buttonSaveKeyFileTEA.TabIndex = 3;
            this.buttonSaveKeyFileTEA.Text = "Save to file...";
            this.buttonSaveKeyFileTEA.UseVisualStyleBackColor = true;
            // 
            // buttonLoadKeyFileTEA
            // 
            this.buttonLoadKeyFileTEA.Location = new System.Drawing.Point(109, 58);
            this.buttonLoadKeyFileTEA.Name = "buttonLoadKeyFileTEA";
            this.buttonLoadKeyFileTEA.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeyFileTEA.TabIndex = 2;
            this.buttonLoadKeyFileTEA.Text = "Load from file...";
            this.buttonLoadKeyFileTEA.UseVisualStyleBackColor = true;
            // 
            // textBoxKeyTEA
            // 
            this.textBoxKeyTEA.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxKeyTEA.Location = new System.Drawing.Point(6, 32);
            this.textBoxKeyTEA.Name = "textBoxKeyTEA";
            this.textBoxKeyTEA.Size = new System.Drawing.Size(999, 20);
            this.textBoxKeyTEA.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(6, 16);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(25, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Key";
            // 
            // tabA4
            // 
            this.tabA4.Controls.Add(this.tableLayoutPanel6);
            this.tabA4.Location = new System.Drawing.Point(4, 22);
            this.tabA4.Name = "tabA4";
            this.tabA4.Size = new System.Drawing.Size(1017, 648);
            this.tabA4.TabIndex = 6;
            this.tabA4.Text = "RSA CBC";
            this.tabA4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.buttonDecryptRSA, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.textBoxPlainTextRSA, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.textBoxEncryptedTextRSA, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.buttonEncryptRSA, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label16, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.groupBox5, 0, 3);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1017, 648);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // buttonDecryptRSA
            // 
            this.buttonDecryptRSA.Location = new System.Drawing.Point(511, 428);
            this.buttonDecryptRSA.Name = "buttonDecryptRSA";
            this.buttonDecryptRSA.Size = new System.Drawing.Size(180, 37);
            this.buttonDecryptRSA.TabIndex = 3;
            this.buttonDecryptRSA.Text = "Decrypt";
            this.buttonDecryptRSA.UseVisualStyleBackColor = true;
            // 
            // textBoxPlainTextRSA
            // 
            this.textBoxPlainTextRSA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxPlainTextRSA.Location = new System.Drawing.Point(3, 27);
            this.textBoxPlainTextRSA.Multiline = true;
            this.textBoxPlainTextRSA.Name = "textBoxPlainTextRSA";
            this.textBoxPlainTextRSA.Size = new System.Drawing.Size(502, 395);
            this.textBoxPlainTextRSA.TabIndex = 0;
            // 
            // textBoxEncryptedTextRSA
            // 
            this.textBoxEncryptedTextRSA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxEncryptedTextRSA.Location = new System.Drawing.Point(511, 27);
            this.textBoxEncryptedTextRSA.Multiline = true;
            this.textBoxEncryptedTextRSA.Name = "textBoxEncryptedTextRSA";
            this.textBoxEncryptedTextRSA.Size = new System.Drawing.Size(503, 395);
            this.textBoxEncryptedTextRSA.TabIndex = 1;
            // 
            // buttonEncryptRSA
            // 
            this.buttonEncryptRSA.Location = new System.Drawing.Point(3, 428);
            this.buttonEncryptRSA.Name = "buttonEncryptRSA";
            this.buttonEncryptRSA.Size = new System.Drawing.Size(180, 37);
            this.buttonEncryptRSA.TabIndex = 2;
            this.buttonEncryptRSA.Text = "Encrypt";
            this.buttonEncryptRSA.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Top;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(502, 24);
            this.label15.TabIndex = 3;
            this.label15.Text = "Plain Text";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Top;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(511, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(503, 24);
            this.label16.TabIndex = 4;
            this.label16.Text = "Encrypted";
            // 
            // groupBox5
            // 
            this.tableLayoutPanel6.SetColumnSpan(this.groupBox5, 2);
            this.groupBox5.Controls.Add(this.buttonLoadKeyRSAPublic);
            this.groupBox5.Controls.Add(this.buttonSaveKeyFileRSAPublic);
            this.groupBox5.Controls.Add(this.buttonLoadKeyFilePublic);
            this.groupBox5.Controls.Add(this.textBoxKeyRSAPublic);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Controls.Add(this.buttonLoadKeyRSAPrivate);
            this.groupBox5.Controls.Add(this.buttonSaveKeyFileRSAPrivate);
            this.groupBox5.Controls.Add(this.buttonLoadKeyFileRSAPrivate);
            this.groupBox5.Controls.Add(this.textBoxKeyRSAPrivate);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(3, 473);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(1011, 172);
            this.groupBox5.TabIndex = 5;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = " Settings ";
            // 
            // buttonLoadKeyRSAPublic
            // 
            this.buttonLoadKeyRSAPublic.Location = new System.Drawing.Point(6, 134);
            this.buttonLoadKeyRSAPublic.Name = "buttonLoadKeyRSAPublic";
            this.buttonLoadKeyRSAPublic.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeyRSAPublic.TabIndex = 9;
            this.buttonLoadKeyRSAPublic.Text = "Load key";
            this.buttonLoadKeyRSAPublic.UseVisualStyleBackColor = true;
            // 
            // buttonSaveKeyFileRSAPublic
            // 
            this.buttonSaveKeyFileRSAPublic.Location = new System.Drawing.Point(212, 134);
            this.buttonSaveKeyFileRSAPublic.Name = "buttonSaveKeyFileRSAPublic";
            this.buttonSaveKeyFileRSAPublic.Size = new System.Drawing.Size(97, 31);
            this.buttonSaveKeyFileRSAPublic.TabIndex = 8;
            this.buttonSaveKeyFileRSAPublic.Text = "Save to file...";
            this.buttonSaveKeyFileRSAPublic.UseVisualStyleBackColor = true;
            // 
            // buttonLoadKeyFilePublic
            // 
            this.buttonLoadKeyFilePublic.Location = new System.Drawing.Point(109, 134);
            this.buttonLoadKeyFilePublic.Name = "buttonLoadKeyFilePublic";
            this.buttonLoadKeyFilePublic.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeyFilePublic.TabIndex = 7;
            this.buttonLoadKeyFilePublic.Text = "Load from file...";
            this.buttonLoadKeyFilePublic.UseVisualStyleBackColor = true;
            // 
            // textBoxKeyRSAPublic
            // 
            this.textBoxKeyRSAPublic.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxKeyRSAPublic.Location = new System.Drawing.Point(6, 108);
            this.textBoxKeyRSAPublic.Name = "textBoxKeyRSAPublic";
            this.textBoxKeyRSAPublic.Size = new System.Drawing.Size(999, 20);
            this.textBoxKeyRSAPublic.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 13);
            this.label5.TabIndex = 5;
            this.label5.Text = "Public key";
            // 
            // buttonLoadKeyRSAPrivate
            // 
            this.buttonLoadKeyRSAPrivate.Location = new System.Drawing.Point(6, 58);
            this.buttonLoadKeyRSAPrivate.Name = "buttonLoadKeyRSAPrivate";
            this.buttonLoadKeyRSAPrivate.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeyRSAPrivate.TabIndex = 4;
            this.buttonLoadKeyRSAPrivate.Text = "Load key";
            this.buttonLoadKeyRSAPrivate.UseVisualStyleBackColor = true;
            // 
            // buttonSaveKeyFileRSAPrivate
            // 
            this.buttonSaveKeyFileRSAPrivate.Location = new System.Drawing.Point(212, 58);
            this.buttonSaveKeyFileRSAPrivate.Name = "buttonSaveKeyFileRSAPrivate";
            this.buttonSaveKeyFileRSAPrivate.Size = new System.Drawing.Size(97, 31);
            this.buttonSaveKeyFileRSAPrivate.TabIndex = 3;
            this.buttonSaveKeyFileRSAPrivate.Text = "Save to file...";
            this.buttonSaveKeyFileRSAPrivate.UseVisualStyleBackColor = true;
            // 
            // buttonLoadKeyFileRSAPrivate
            // 
            this.buttonLoadKeyFileRSAPrivate.Location = new System.Drawing.Point(109, 58);
            this.buttonLoadKeyFileRSAPrivate.Name = "buttonLoadKeyFileRSAPrivate";
            this.buttonLoadKeyFileRSAPrivate.Size = new System.Drawing.Size(97, 31);
            this.buttonLoadKeyFileRSAPrivate.TabIndex = 2;
            this.buttonLoadKeyFileRSAPrivate.Text = "Load from file...";
            this.buttonLoadKeyFileRSAPrivate.UseVisualStyleBackColor = true;
            // 
            // textBoxKeyRSAPrivate
            // 
            this.textBoxKeyRSAPrivate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxKeyRSAPrivate.Location = new System.Drawing.Point(6, 32);
            this.textBoxKeyRSAPrivate.Name = "textBoxKeyRSAPrivate";
            this.textBoxKeyRSAPrivate.Size = new System.Drawing.Size(999, 20);
            this.textBoxKeyRSAPrivate.TabIndex = 1;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(6, 16);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 0;
            this.label17.Text = "Private key";
            // 
            // tabFSW
            // 
            this.tabFSW.Controls.Add(this.groupBox1);
            this.tabFSW.Location = new System.Drawing.Point(4, 22);
            this.tabFSW.Name = "tabFSW";
            this.tabFSW.Padding = new System.Windows.Forms.Padding(3);
            this.tabFSW.Size = new System.Drawing.Size(1017, 648);
            this.tabFSW.TabIndex = 1;
            this.tabFSW.Text = "File System Watcher Crypt";
            this.tabFSW.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1011, 642);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Settings";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.86487F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.13513F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 111F));
            this.tableLayoutPanel2.Controls.Add(this.textBoxSourceDir, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnSourceDirSelect, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.textBoxDestDir, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnDestinationDirSelect, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.buttonStopFSW, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.buttonStartFSW, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.buttonDecryptAll, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.textBoxConsoleFSW, 0, 5);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1005, 623);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // textBoxSourceDir
            // 
            this.textBoxSourceDir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxSourceDir.Location = new System.Drawing.Point(135, 5);
            this.textBoxSourceDir.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.textBoxSourceDir.Name = "textBoxSourceDir";
            this.textBoxSourceDir.ReadOnly = true;
            this.textBoxSourceDir.Size = new System.Drawing.Size(755, 20);
            this.textBoxSourceDir.TabIndex = 1;
            // 
            // btnSourceDirSelect
            // 
            this.btnSourceDirSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnSourceDirSelect.Location = new System.Drawing.Point(896, 3);
            this.btnSourceDirSelect.Name = "btnSourceDirSelect";
            this.btnSourceDirSelect.Size = new System.Drawing.Size(106, 23);
            this.btnSourceDirSelect.TabIndex = 2;
            this.btnSourceDirSelect.Text = "Select...";
            this.btnSourceDirSelect.UseVisualStyleBackColor = true;
            this.btnSourceDirSelect.Click += new System.EventHandler(this.btnSourceDirSelect_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
            this.label4.Size = new System.Drawing.Size(126, 29);
            this.label4.TabIndex = 3;
            this.label4.Text = "Source Dir";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Location = new System.Drawing.Point(3, 29);
            this.label7.Name = "label7";
            this.label7.Padding = new System.Windows.Forms.Padding(0, 7, 0, 0);
            this.label7.Size = new System.Drawing.Size(126, 29);
            this.label7.TabIndex = 4;
            this.label7.Text = "Destination Dir";
            // 
            // textBoxDestDir
            // 
            this.textBoxDestDir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxDestDir.Location = new System.Drawing.Point(135, 32);
            this.textBoxDestDir.Name = "textBoxDestDir";
            this.textBoxDestDir.ReadOnly = true;
            this.textBoxDestDir.Size = new System.Drawing.Size(755, 20);
            this.textBoxDestDir.TabIndex = 5;
            // 
            // btnDestinationDirSelect
            // 
            this.btnDestinationDirSelect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnDestinationDirSelect.Location = new System.Drawing.Point(896, 32);
            this.btnDestinationDirSelect.Name = "btnDestinationDirSelect";
            this.btnDestinationDirSelect.Size = new System.Drawing.Size(106, 23);
            this.btnDestinationDirSelect.TabIndex = 6;
            this.btnDestinationDirSelect.Text = "Select...";
            this.btnDestinationDirSelect.UseVisualStyleBackColor = true;
            this.btnDestinationDirSelect.Click += new System.EventHandler(this.btnDestinationDirSelect_Click);
            // 
            // buttonStopFSW
            // 
            this.buttonStopFSW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonStopFSW.Enabled = false;
            this.buttonStopFSW.Location = new System.Drawing.Point(135, 107);
            this.buttonStopFSW.Name = "buttonStopFSW";
            this.buttonStopFSW.Size = new System.Drawing.Size(755, 40);
            this.buttonStopFSW.TabIndex = 7;
            this.buttonStopFSW.Text = "STOP";
            this.buttonStopFSW.UseVisualStyleBackColor = true;
            this.buttonStopFSW.Click += new System.EventHandler(this.buttonStopFSW_Click);
            // 
            // buttonStartFSW
            // 
            this.buttonStartFSW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonStartFSW.Enabled = false;
            this.buttonStartFSW.Location = new System.Drawing.Point(135, 61);
            this.buttonStartFSW.Name = "buttonStartFSW";
            this.buttonStartFSW.Size = new System.Drawing.Size(755, 40);
            this.buttonStartFSW.TabIndex = 8;
            this.buttonStartFSW.Text = "START";
            this.buttonStartFSW.UseVisualStyleBackColor = true;
            this.buttonStartFSW.Click += new System.EventHandler(this.buttonStartFSW_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Location = new System.Drawing.Point(3, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 29);
            this.label6.TabIndex = 10;
            this.label6.Text = "FSW Output";
            // 
            // buttonDecryptAll
            // 
            this.buttonDecryptAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonDecryptAll.Enabled = false;
            this.buttonDecryptAll.Location = new System.Drawing.Point(896, 153);
            this.buttonDecryptAll.Name = "buttonDecryptAll";
            this.buttonDecryptAll.Size = new System.Drawing.Size(106, 23);
            this.buttonDecryptAll.TabIndex = 11;
            this.buttonDecryptAll.Text = "DecryptAll";
            this.buttonDecryptAll.UseVisualStyleBackColor = true;
            this.buttonDecryptAll.Click += new System.EventHandler(this.buttonDecryptAll_Click);
            // 
            // textBoxConsoleFSW
            // 
            this.textBoxConsoleFSW.BackColor = System.Drawing.SystemColors.ControlText;
            this.tableLayoutPanel2.SetColumnSpan(this.textBoxConsoleFSW, 3);
            this.textBoxConsoleFSW.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBoxConsoleFSW.ForeColor = System.Drawing.SystemColors.Window;
            this.textBoxConsoleFSW.Location = new System.Drawing.Point(3, 182);
            this.textBoxConsoleFSW.Multiline = true;
            this.textBoxConsoleFSW.Name = "textBoxConsoleFSW";
            this.textBoxConsoleFSW.ReadOnly = true;
            this.textBoxConsoleFSW.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxConsoleFSW.Size = new System.Drawing.Size(999, 438);
            this.textBoxConsoleFSW.TabIndex = 12;
            // 
            // tabSimulation
            // 
            this.tabSimulation.Location = new System.Drawing.Point(4, 22);
            this.tabSimulation.Name = "tabSimulation";
            this.tabSimulation.Size = new System.Drawing.Size(1017, 648);
            this.tabSimulation.TabIndex = 2;
            this.tabSimulation.Text = "Simulation";
            this.tabSimulation.UseVisualStyleBackColor = true;
            // 
            // tabMessenger
            // 
            this.tabMessenger.Controls.Add(this.tableLayoutPanel3);
            this.tabMessenger.Location = new System.Drawing.Point(4, 22);
            this.tabMessenger.Name = "tabMessenger";
            this.tabMessenger.Size = new System.Drawing.Size(1017, 648);
            this.tabMessenger.TabIndex = 7;
            this.tabMessenger.Text = "Messenger";
            this.tabMessenger.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panelSendMessage, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1017, 648);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonNickname);
            this.panel1.Controls.Add(this.textBoxUsername);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.textBoxReceivedMessages);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1011, 318);
            this.panel1.TabIndex = 0;
            // 
            // buttonNickname
            // 
            this.buttonNickname.Location = new System.Drawing.Point(234, 8);
            this.buttonNickname.Name = "buttonNickname";
            this.buttonNickname.Size = new System.Drawing.Size(64, 23);
            this.buttonNickname.TabIndex = 4;
            this.buttonNickname.Text = "Login";
            this.buttonNickname.UseVisualStyleBackColor = true;
            this.buttonNickname.Click += new System.EventHandler(this.buttonNickname_Click);
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(69, 10);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(159, 20);
            this.textBoxUsername.TabIndex = 3;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(5, 13);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(58, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Nickname:";
            // 
            // textBoxReceivedMessages
            // 
            this.textBoxReceivedMessages.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxReceivedMessages.Location = new System.Drawing.Point(6, 49);
            this.textBoxReceivedMessages.Multiline = true;
            this.textBoxReceivedMessages.Name = "textBoxReceivedMessages";
            this.textBoxReceivedMessages.ReadOnly = true;
            this.textBoxReceivedMessages.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxReceivedMessages.Size = new System.Drawing.Size(1000, 266);
            this.textBoxReceivedMessages.TabIndex = 1;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(5, 33);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 13);
            this.label8.TabIndex = 0;
            this.label8.Text = "Received messages";
            // 
            // panelSendMessage
            // 
            this.panelSendMessage.Controls.Add(this.listBoxAvailableUsers);
            this.panelSendMessage.Controls.Add(this.label18);
            this.panelSendMessage.Controls.Add(this.buttonSend);
            this.panelSendMessage.Controls.Add(this.textBoxMessage);
            this.panelSendMessage.Controls.Add(this.lable19);
            this.panelSendMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelSendMessage.Enabled = false;
            this.panelSendMessage.Location = new System.Drawing.Point(3, 327);
            this.panelSendMessage.Name = "panelSendMessage";
            this.panelSendMessage.Size = new System.Drawing.Size(1011, 318);
            this.panelSendMessage.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(5, 14);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Receivers";
            // 
            // buttonSend
            // 
            this.buttonSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.buttonSend.Location = new System.Drawing.Point(8, 279);
            this.buttonSend.Name = "buttonSend";
            this.buttonSend.Size = new System.Drawing.Size(199, 32);
            this.buttonSend.TabIndex = 2;
            this.buttonSend.Text = "Send";
            this.buttonSend.UseVisualStyleBackColor = true;
            this.buttonSend.Click += new System.EventHandler(this.buttonSend_Click);
            // 
            // textBoxMessage
            // 
            this.textBoxMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxMessage.Location = new System.Drawing.Point(8, 253);
            this.textBoxMessage.Name = "textBoxMessage";
            this.textBoxMessage.Size = new System.Drawing.Size(1000, 20);
            this.textBoxMessage.TabIndex = 1;
            // 
            // lable19
            // 
            this.lable19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lable19.AutoSize = true;
            this.lable19.Location = new System.Drawing.Point(5, 237);
            this.lable19.Name = "lable19";
            this.lable19.Size = new System.Drawing.Size(50, 13);
            this.lable19.TabIndex = 0;
            this.lable19.Text = "Message";
            // 
            // openFileDialogKey
            // 
            this.openFileDialogKey.Title = "Select ZIKey file";
            // 
            // saveFileDialogKey
            // 
            this.saveFileDialogKey.DefaultExt = "zikey";
            this.saveFileDialogKey.Title = "Choose ZIKey save location";
            // 
            // TrayIcon
            // 
            this.TrayIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("TrayIcon.Icon")));
            this.TrayIcon.Text = "ZICrypter";
            this.TrayIcon.Visible = true;
            // 
            // listBoxAvailableUsers
            // 
            this.listBoxAvailableUsers.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxAvailableUsers.FormattingEnabled = true;
            this.listBoxAvailableUsers.Location = new System.Drawing.Point(8, 30);
            this.listBoxAvailableUsers.Name = "listBoxAvailableUsers";
            this.listBoxAvailableUsers.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.listBoxAvailableUsers.Size = new System.Drawing.Size(998, 199);
            this.listBoxAvailableUsers.TabIndex = 4;
            this.listBoxAvailableUsers.Click += new System.EventHandler(this.listBoxAvailableUsers_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1025, 720);
            this.Controls.Add(this.tabControl);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MinimumSize = new System.Drawing.Size(730, 630);
            this.Name = "MainForm";
            this.Text = "ZI Crypter";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tabA1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.groupBoxSettings.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewSSKey)).EndInit();
            this.tabA2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.tabA3.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabA4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabFSW.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tabMessenger.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panelSendMessage.ResumeLayout(false);
            this.panelSendMessage.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabA1;
        private System.Windows.Forms.TabPage tabFSW;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TextBox textBoxSourceDir;
        private System.Windows.Forms.Button btnSourceDirSelect;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.OpenFileDialog openFileDialogKey;
        private System.Windows.Forms.SaveFileDialog saveFileDialogKey;
        private System.Windows.Forms.TextBox textBoxDestDir;
        private System.Windows.Forms.Button btnDestinationDirSelect;
        private System.Windows.Forms.TabPage tabSimulation;
        private System.Windows.Forms.ToolStripMenuItem manualToolStripMenuItem;
        private System.Windows.Forms.TabPage tabA2;
        private System.Windows.Forms.TabPage tabA3;
        private System.Windows.Forms.TabPage tabA4;
        private System.Windows.Forms.TabPage tabMessenger;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Button buttonDecryptTEA;
        private System.Windows.Forms.TextBox textBoxPlainTextTEA;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Button buttonEncryptTEA;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button buttonLoadKeyTEA;
        private System.Windows.Forms.Button buttonSaveKeyFileTEA;
        private System.Windows.Forms.Button buttonLoadKeyFileTEA;
        private System.Windows.Forms.TextBox textBoxKeyTEA;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.Button buttonDecryptRSA;
        private System.Windows.Forms.TextBox textBoxPlainTextRSA;
        private System.Windows.Forms.TextBox textBoxEncryptedTextRSA;
        private System.Windows.Forms.Button buttonEncryptRSA;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button buttonLoadKeyRSAPrivate;
        private System.Windows.Forms.Button buttonSaveKeyFileRSAPrivate;
        private System.Windows.Forms.Button buttonLoadKeyFileRSAPrivate;
        private System.Windows.Forms.TextBox textBoxKeyRSAPrivate;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button buttonDecryptSS;
        private System.Windows.Forms.TextBox textBoxPlainTextSS;
        private System.Windows.Forms.TextBox textBoxEncryptedTextSS;
        private System.Windows.Forms.Button buttonEncryptSS;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBoxSettings;
        private System.Windows.Forms.Button buttonLoadKeySS;
        private System.Windows.Forms.Button buttonSaveKeyFileSS;
        private System.Windows.Forms.Button buttonLoadKeyFileSS;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Button buttonDecryptA51;
        private System.Windows.Forms.TextBox textBoxPlainTextA51;
        private System.Windows.Forms.TextBox textBoxEncryptedTextA51;
        private System.Windows.Forms.Button buttonEncryptA51;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button buttonLoadKeyA51;
        private System.Windows.Forms.Button buttonSaveKeyFileA51;
        private System.Windows.Forms.Button buttonLoadKeyFileA51;
        private System.Windows.Forms.TextBox textBoxKeyA51;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button buttonLoadKeyRSAPublic;
        private System.Windows.Forms.Button buttonSaveKeyFileRSAPublic;
        private System.Windows.Forms.Button buttonLoadKeyFilePublic;
        private System.Windows.Forms.TextBox textBoxKeyRSAPublic;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button buttonStopFSW;
        private System.Windows.Forms.Button buttonStartFSW;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button buttonDecryptAll;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox textBoxReceivedMessages;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panelSendMessage;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button buttonSend;
        private System.Windows.Forms.TextBox textBoxMessage;
        private System.Windows.Forms.Label lable19;
        private System.Windows.Forms.NotifyIcon TrayIcon;
        private System.Windows.Forms.Button buttonNickname;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.DataGridView dataGridViewSSKey;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusMessage;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxConsoleFSW;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ComboBox comboBoxMajorityZ;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBoxMajorityY;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox comboBoxMajorityX;
        private System.Windows.Forms.TextBox textBoxShiftGenZ;
        private System.Windows.Forms.TextBox textBoxShiftGenY;
        private System.Windows.Forms.TextBox textBoxShiftGenX;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ListBox listBoxAvailableUsers;
    }
}

