﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.FSW;
using ZICrypter.FSW.EventArgs;

namespace ZICrypter.Controllers
{
    class FSWController
    {
        #region Events
        //todo: Find better way, without 'wrapper redirection';
        public event FSWCrytper.FSWCrypterEventHandler OnFSWOutput;
        private void Fsw_OnFSWEvent(object sender, FSWEventArg e)
        {
            e.Message = $"[{DateTime.Now}] {e.Message}{Environment.NewLine}";
            OnFSWOutput?.Invoke(sender, e);
        }
        #endregion

        public readonly string  DefaultLocation = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos);

        private readonly FSWCrytper _fsw;
        public FSWCrytper.FSWState State => _fsw.State;

        public string SourceDirectory
        {
            get => _fsw.SourceDir;
            set => _fsw.SourceDir = value;
        }

        public string DestinationDirectory
        {
            get => _fsw.DestinationDir;
            set => _fsw.DestinationDir = value;
        }

        public FSWController()
        {
            _fsw = new FSWCrytper(DefaultLocation, DefaultLocation);
            _fsw.OnFSWEvent += Fsw_OnFSWEvent;
        }

        public void Start()
        {
            _fsw.StartRestored();
        }
        public void Stop()
        {
            if (_fsw.State != FSWCrytper.FSWState.Running)
                return;
            _fsw.StopAndSave();
        }
        public void DecryptAll()
        {
            _fsw.DecryptAll();
        }

        public void Shutdown()
        {
            _fsw.OnFSWEvent -= Fsw_OnFSWEvent;
            Stop();
        }
    }
}
