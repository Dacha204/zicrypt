﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.Controllers.CryptControllers;

namespace ZICrypter.Controllers
{
    class ZIController
    {
        #region ZIController Singleton
        private static ZIController _instance;
        private static readonly object Padlock = new object();
        public static ZIController Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new ZIController());
                }
            }
        }
        private ZIController()
        {
            SimpleSubstitution = new SimpleSubstitutionController();
            A51 = new A51Controller();
            RSA = new RSAController();

            FSW = new FSWController();
            Communicator = new ComunicatorController();
        }
        #endregion

        #region CryptoControllers
        public SimpleSubstitutionController SimpleSubstitution { get; protected set; }
        public A51Controller A51 { get; protected set; }
        public RSAController RSA { get; protected set; }
        #endregion

        public FSWController FSW { get; protected set; }
        public ComunicatorController Communicator { get; protected set; }
    }
}
