﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.Controllers.CryptControllers.Abstract;
using ZICrypter.CryptoLib.CryptoHandlers;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.Controllers.CryptControllers
{
    class RSAController : AsymetricCryptController
    {
        protected override PublicCryptoKey CreatePublicKey()
        {
            return new RSAPublicKey();
        }
        protected override PrivateCryptoKey CreatePrivateKey()
        {
            return new RSAPrivateKey();
        }
        protected override EncryptionHandler CreateEncrypter()
        {
            return new EncryptionHandler(CryptoHandler.CryptoAlgorithm.RSA);
        }
        protected override DecryptionHandler CreateDecrypter()
        {
            return new DecryptionHandler(CryptoHandler.CryptoAlgorithm.RSA);
        }
    }
}
