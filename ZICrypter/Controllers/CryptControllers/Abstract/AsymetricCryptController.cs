﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.Controllers.CryptControllers.Abstract
{
    abstract class AsymetricCryptController : CryptController
    {

        public virtual void SetPublicKey(string keyString)
        {
            PublicCryptoKey newKey = CreatePublicKey();
            newKey.ParseKey(keyString);
            encrypter.Key = newKey;
        }
        public virtual void SetPublicKeyFromFile(string fileName)
        {
            PublicCryptoKey newKey = CreatePublicKey();
            newKey.LoadFromFile(fileName);
            encrypter.Key = newKey;
        }
        public virtual void SavePublicKeyToFile(string fileName)
        {
            encrypter.Key.SaveToFile(fileName);
        }

        public virtual void SetPrivateKey(string keyString)
        {
            PrivateCryptoKey newKey = CreatePrivateKey();
            newKey.ParseKey(keyString);
            decrypter.Key = newKey;
        }
        public virtual void SetPrivateKeyFromFile(string fileName)
        {
            PrivateCryptoKey newKey = CreatePrivateKey();
            newKey.LoadFromFile(fileName);
            decrypter.Key = newKey;
        }
        public virtual void SavePrivateKeyToFile(string fileName)
        {
            decrypter.Key.SaveToFile(fileName);
        }

        protected abstract PublicCryptoKey CreatePublicKey();
        protected abstract PrivateCryptoKey CreatePrivateKey();

        public virtual string GetPublicKeyString()
        {
            return encrypter.Key.ToString();
        }
        public virtual string GetPrivateKeyString()
        {
            return decrypter.Key.ToString();
        }
    }
}
