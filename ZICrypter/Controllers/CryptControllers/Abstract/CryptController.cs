﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.CryptoHandlers;

namespace ZICrypter.Controllers.CryptControllers.Abstract
{
    abstract class CryptController
    {
        protected EncryptionHandler encrypter;
        protected DecryptionHandler decrypter;

        protected CryptController()
        {
            encrypter = CreateEncrypter();
            decrypter = CreateDecrypter();
        }
        public virtual string Encrypt(string plainText)
        {
            if(encrypter == null)
                throw new Exception("Encrypter not initialized!");

            string encryptedText = plainText;
            encrypter.PerformCrypt(ref encryptedText);
            return encryptedText;
        }
        public virtual string Decrypt(string encryptedText)
        {
            if(decrypter == null)
                throw new Exception("Decrypter not initilazied");

            string decryptedText = encryptedText;
            decrypter.PerformCrypt(ref decryptedText);
            return decryptedText;
        }

        protected abstract EncryptionHandler CreateEncrypter();
        protected abstract DecryptionHandler CreateDecrypter();
    }
}
