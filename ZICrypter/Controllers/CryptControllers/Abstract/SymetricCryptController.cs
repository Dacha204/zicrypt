﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.Controllers.CryptControllers.Abstract
{
    abstract class SymetricCryptController : CryptController
    {
        public virtual void SetKey(string keyString)
        {
            SymetricCrytoKey newKey = CreateKey();
            newKey.ParseKey(keyString);
            encrypter.Key = newKey;
            decrypter.Key = newKey;
        }
        public virtual void SetKeyFromFile(string fileName)
        {
            SymetricCrytoKey newKey = CreateKey();
            newKey.LoadFromFile(fileName);
            encrypter.Key = newKey;
            decrypter.Key = newKey;
        }
        public virtual void SaveKeyToFile(string fileName)
        {
            encrypter.Key.SaveToFile(fileName);
        }

        protected abstract SymetricCrytoKey CreateKey();

        public virtual string GetKeyString()
        {
            return encrypter.Key.ToString();
        }
    }
}
