﻿using System;
using System.Collections.Generic;
using ZICrypter.Controllers.CryptControllers.Abstract;
using ZICrypter.CryptoLib.Crypters;
using ZICrypter.CryptoLib.CryptoHandlers;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.Controllers.CryptControllers
{
    class SimpleSubstitutionController : SymetricCryptController
    {
        protected override SymetricCrytoKey CreateKey()
        {
            return new SimpleSubstitutionKey();
        }
        protected override EncryptionHandler CreateEncrypter()
        {
            return new EncryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution);
        }
        protected override DecryptionHandler CreateDecrypter()
        {
            return new DecryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution);
        }
    }
}
