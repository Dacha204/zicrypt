﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.Controllers.CryptControllers.Abstract;
using ZICrypter.CryptoLib.CryptoHandlers;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.CryptoLib.CryptoKeys.AbstactKey;

namespace ZICrypter.Controllers.CryptControllers
{
    class A51Controller : SymetricCryptController
    {
        protected override SymetricCrytoKey CreateKey()
        {
            return new A51Key();
        }
        protected override EncryptionHandler CreateEncrypter()
        {
            return new EncryptionHandler(CryptoHandler.CryptoAlgorithm.A51);
        }
        protected override DecryptionHandler CreateDecrypter()
        {
            return new DecryptionHandler(CryptoHandler.CryptoAlgorithm.A51);
        }
    }
}
