﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZICrypter.Communicator;
using ZICrypter.Communicator.EventArg;
using ZICrypter.Communicator.Model;
using ZICrypter.CryptoLib.CryptoHandlers;
using ZICrypter.CryptoLib.CryptoKeys;

namespace ZICrypter.Controllers
{
    class ComunicatorController
    {
        private EncryptionHandler _encrypter;
        private DecryptionHandler _decrypter;

        public event ZICommunicator.NewOnlineClientHandler OnOnlineClientsChange ;
        public event ZICommunicator.MessageReceivedHandler OnNewMessage;
        private void CommunicatorOnOnNewMessage(object sender, MessageReceivedEventArgs e)
        {
            string decryptedMessage = e.Message.MessageText;
            _decrypter.PerformCrypt(ref decryptedMessage);
            e.Message.MessageText = decryptedMessage;

            OnNewMessage?.Invoke(sender, e);
        }
        private void CommunicatorOnOnlineClientsChange(object sender, Communicator.EventArg.NewOnlineEventArgs e)
        {
            OnOnlineClientsChange?.Invoke(sender, e);
        }


        private ZICommunicator _communicator;
        public string Username { get; set; }
        public List<Client> Receivers { get; set; }
        public List<Client> AvailableUsers => _communicator.Clients;

        public ComunicatorController()
        {
            Receivers = new List<Client>();
            Random randomGen = new Random(DateTime.Now.Millisecond);
            Username = "ZICryptUser_" + randomGen.Next(1000);

            SimpleSubstitutionKey key = new SimpleSubstitutionKey();
            for (int i = 0; i < 256; i++)
            {
                key.SubstitutionMap.Add((byte) i, (byte) ~i);
            }

            _encrypter = new EncryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution, key);
            _decrypter = new DecryptionHandler(CryptoHandler.CryptoAlgorithm.SimpleSubstitution, key);
        }

        public void Login()
        {
            if (_communicator != null)
                CommunicatonAbort("Already loged in!");

            InitCommunicator();   
        }
        public void SignOut()
        {
            if (_communicator == null)
                return;

            _communicator.Dispose();
            _communicator = null;
        }
        
        public void SendMessage(string messageText)
        {
            if (_communicator == null)
                CommunicatonAbort("Can not send message. User not loged in!");

            string encryptedText = messageText;
            _encrypter.PerformCrypt(ref encryptedText);

            _communicator.SendMessage(encryptedText, Receivers);
        }
        
        private void InitCommunicator()
        {
            _communicator = new ZICommunicator(Username);
            _communicator.OnNewOnlineClient += CommunicatorOnOnlineClientsChange;
            _communicator.OnNewMessage += CommunicatorOnOnNewMessage;
        }
        private void CommunicatonAbort(string reasonMessage)
        {
            SignOut();
            throw new Exception(reasonMessage);
        }
    }
}
