﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using ZICrypter.Communicator;
using ZICrypter.Communicator.EventArg;
using ZICrypter.Communicator.Model;
using ZICrypter.Controllers;
using ZICrypter.CryptoLib.CryptoKeys;
using ZICrypter.FSW;


namespace ZICrypter
{
    public partial class MainForm : Form
    {

        public MainForm()
        {
            InitializeComponent();
        }
        private void MainForm_Load(object sender, EventArgs e)
        {
            InitSimpleSubstitutionSettings();
            InitA51Settings();
            InitFSWSettings();
            InitMessengerSettings();
        }

        #region SimpleSubstitutionTab
        private void InitSimpleSubstitutionSettings()
        {
            dataGridViewSSKey.Columns.Add("Element", "Element");
            dataGridViewSSKey.Columns.Add("Replacement", "Replacement");
            dataGridViewSSKey.Columns[0].ReadOnly = true;

            string symbols = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.! ,-+()[];:'" +
                             "abcdefghijklmnopqrstuvwxyz";

            dataGridViewSSKey.Rows.Add(symbols.Length);
            int i = 0;
            foreach (DataGridViewRow row in dataGridViewSSKey.Rows)
            {
                row.Cells[0].Value = symbols[i++];
            }
        }
        private void EnableSimpleSubstitutionCrypting()
        {
            buttonSaveKeyFileSS.Enabled = true;
            textBoxPlainTextSS.Enabled = true;
            textBoxEncryptedTextSS.Enabled = true;
            buttonEncryptSS.Enabled = true;
            buttonDecryptSS.Enabled = true;
        }
        private void FillSimpleSubstitutionGrid(string keyString)
        {
            dataGridViewSSKey.Rows.Clear();
            
            char[] separators = { '|' };
            string[] substitutions = keyString.Split(separators, StringSplitOptions.RemoveEmptyEntries);

            dataGridViewSSKey.Rows.Add(substitutions.Length);
            int i = 0;
            foreach (string substitution in substitutions)
            {
                dataGridViewSSKey.Rows[i].Cells[0].Value = substitution[0];
                dataGridViewSSKey.Rows[i].Cells[1].Value = substitution[1];
                i++;
            }
        }

        private void buttonLoadKeySS_Click(object sender, EventArgs e)
        {
            try
            {
                string keyString = string.Empty;

                foreach (DataGridViewRow row in dataGridViewSSKey.Rows)
                {
                    if (row.Cells[1].Value == null)
                        throw new Exception($"Missing replacement for {row.Cells[0].Value}");

                    string element = row.Cells[0].Value.ToString();
                    string replacement = row.Cells[1].Value.ToString();

                    keyString += $"{element}{replacement}|";
                }

                ZIController.Instance.SimpleSubstitution.SetKey(keyString);

                EnableSimpleSubstitutionCrypting();
                toolStripStatusMessage.Text = "Simple Substitution key successfully loaded";
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        private void buttonSaveKeyFileSS_Click(object sender, EventArgs e)
        {
            saveFileDialogKey.FileName = "SimpleSubstitution";
            DialogResult result = saveFileDialogKey.ShowDialog();
            if (result != DialogResult.OK)
                return;

            try
            {
                ZIController.Instance.SimpleSubstitution.SaveKeyToFile(saveFileDialogKey.FileName);

                toolStripStatusMessage.Text = "Simple Substitution key successfully saved to file";
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        private void buttonLoadKeyFileSS_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialogKey.ShowDialog();
            if (result != DialogResult.OK)
                return;

            try
            {
                ZIController.Instance.SimpleSubstitution.SetKeyFromFile(openFileDialogKey.FileName);

                EnableSimpleSubstitutionCrypting();
                FillSimpleSubstitutionGrid(ZIController.Instance.SimpleSubstitution.GetKeyString());
                toolStripStatusMessage.Text = "Simple Substitution key successfully loaded from file.";
            }
            catch (Exception ex)
            {
                ShowMessage("Failed to load key. Check key type. \n\n Details: " + ex.Message, MessageBoxIcon.Error);
            }
        }
        private void buttonEncryptSS_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxEncryptedTextSS.Text = ZIController.Instance.SimpleSubstitution.Encrypt(textBoxPlainTextSS.Text);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        private void buttonDecryptSS_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxPlainTextSS.Text = ZIController.Instance.SimpleSubstitution.Decrypt(textBoxEncryptedTextSS.Text);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        #endregion
        
        #region A51Tab
        private void InitA51Settings()
        {
            for (int i = 0; i < 19; i++)
                comboBoxMajorityX.Items.Add(i);

            for (int i = 0; i < 22; i++)
                comboBoxMajorityY.Items.Add(i);

            for (int i = 0; i < 23; i++)
                comboBoxMajorityZ.Items.Add(i);

            comboBoxMajorityX.SelectedIndex = 8;
            comboBoxMajorityY.SelectedIndex = 10;
            comboBoxMajorityZ.SelectedIndex = 10;
        }
        private void EnableA51Crypting()
        {
            buttonSaveKeyFileA51.Enabled = true;
            textBoxPlainTextA51.Enabled = true;
            textBoxEncryptedTextA51.Enabled = true;
            buttonEncryptA51.Enabled = true;
            buttonDecryptA51.Enabled = true;
        }
        private void FillA51KeyFields(string keyString)
        {
            string[] sections = keyString.Split('|');
            textBoxKeyA51.Text = sections[0];

            string[] majorities = sections[1].Split(';');
            comboBoxMajorityX.SelectedIndex = int.Parse(majorities[0]);
            comboBoxMajorityY.SelectedIndex = int.Parse(majorities[1]);
            comboBoxMajorityZ.SelectedIndex = int.Parse(majorities[2]);

            string[] shifters = sections[2].Split(';');
            textBoxShiftGenX.Text = shifters[0];
            textBoxShiftGenY.Text = shifters[1];
            textBoxShiftGenZ.Text = shifters[2];
        }

        private void buttonLoadKeyA51_Click(object sender, EventArgs e)
        {
            try
            {
                string keyString = $"{textBoxKeyA51.Text}|" +
                                   $"{comboBoxMajorityX.Text};{comboBoxMajorityY.Text};{comboBoxMajorityZ.Text}|" +
                                   $"{textBoxShiftGenX.Text};{textBoxShiftGenY.Text};{textBoxShiftGenZ.Text}";

                ZIController.Instance.A51.SetKey(keyString);
                
                EnableA51Crypting();
                toolStripStatusMessage.Text = "A51 key successfully loaded";
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        private void buttonSaveKeyFileA51_Click(object sender, EventArgs e)
        {
            saveFileDialogKey.FileName = "SimpleSubstitution";
            DialogResult result = saveFileDialogKey.ShowDialog();
            if (result != DialogResult.OK)
                return;

            try
            {
                ZIController.Instance.A51.SaveKeyToFile(saveFileDialogKey.FileName);
                
                toolStripStatusMessage.Text = "A51 key successfully saved to file";
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        private void buttonLoadKeyFileA51_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialogKey.ShowDialog();
            if (result != DialogResult.OK)
                return;

            try
            {
                ZIController.Instance.A51.SetKeyFromFile(openFileDialogKey.FileName);
                
                EnableA51Crypting();
                FillA51KeyFields(ZIController.Instance.A51.GetKeyString());
                toolStripStatusMessage.Text = "A51 key successfully loaded from file.";
            }
            catch (Exception ex)
            {
                ShowMessage("Failed to load key. Check key type. \n\n Details: " + ex.Message, MessageBoxIcon.Error);
            }
        }
        private void buttonEncryptA51_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxEncryptedTextA51.Text = ZIController.Instance.A51.Encrypt(textBoxPlainTextA51.Text);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        private void buttonDecryptA51_Click(object sender, EventArgs e)
        {
            try
            {
                textBoxPlainTextA51.Text = ZIController.Instance.A51.Decrypt(textBoxEncryptedTextA51.Text);
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region FSWTab
        private void InitFSWSettings()
        {
            ZIController.Instance.FSW.OnFSWOutput += FSW_OnFSWOutput;


            //todo Remove later
            ZIController.Instance.FSW.SourceDirectory = "F:\\ZICryptDir";
            ZIController.Instance.FSW.DestinationDirectory = "F:\\ZICryptDir\\Encrypted";
            textBoxDestDir.Text = ZIController.Instance.FSW.DestinationDirectory;
            textBoxSourceDir.Text = ZIController.Instance.FSW.SourceDirectory;
            EnableFSWButtons();
        }
        private void FSW_OnFSWOutput(object sender, FSW.EventArgs.FSWEventArg e)
        {
            if (textBoxConsoleFSW.InvokeRequired)
                textBoxConsoleFSW.Invoke(new Action(() => textBoxConsoleFSW.AppendText(e.Message)));
            else
                textBoxConsoleFSW.AppendText(e.Message);
        }
        private void EnableFSWButtons()
        {
            if (ZIController.Instance.FSW.DestinationDirectory == null ||
                ZIController.Instance.FSW.SourceDirectory == null)
                return;

            buttonStartFSW.Enabled = true;
        }

        private void buttonStartFSW_Click(object sender, EventArgs e)
        {
            try
            {
                ZIController.Instance.FSW.Start();

                buttonStopFSW.Enabled = true;
                buttonStartFSW.Enabled = false;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        private void buttonStopFSW_Click(object sender, EventArgs e)
        {
            try
            {
                ZIController.Instance.FSW.Stop();

                buttonStopFSW.Enabled = false;
                buttonStartFSW.Enabled = true;
                buttonDecryptAll.Enabled = true;
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }
        
        private void buttonDecryptAll_Click(object sender, EventArgs e)
        {
            try
            {
                ZIController.Instance.FSW.DecryptAll();
            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
            }
        }

        private void btnSourceDirSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            DialogResult result = fd.ShowDialog();
            if (result != DialogResult.OK)
                return;

            textBoxSourceDir.Text = fd.SelectedPath;
            ZIController.Instance.FSW.SourceDirectory = fd.SelectedPath;
            EnableFSWButtons();
        }
        private void btnDestinationDirSelect_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fd = new FolderBrowserDialog();
            DialogResult result = fd.ShowDialog();
            if (result != DialogResult.OK)
                return;

            textBoxDestDir.Text = fd.SelectedPath;
            ZIController.Instance.FSW.DestinationDirectory = fd.SelectedPath;
            EnableFSWButtons();
        }
        #endregion

        #region Utilities
        private void ShowMessage(string message, MessageBoxIcon icon)
        {
            string title;

            switch (icon)
            {
                case MessageBoxIcon.Error:
                    title = "Error";
                    break;
                case MessageBoxIcon.Exclamation:
                    title = "Warning";
                    break;
                case MessageBoxIcon.Information:
                case MessageBoxIcon.None:
                    title = "Info";
                    break;
                case MessageBoxIcon.Question:
                    title = "Question";
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(icon), icon, null);
            }

            MessageBox.Show(message, title, MessageBoxButtons.OK, icon);
        }
        #endregion


        #region MessengerTab
        private void InitMessengerSettings()
        {
            textBoxUsername.Text = ZIController.Instance.Communicator.Username;
        }
        private void ShowTrayIconSignedIn()
        {
            TrayIcon.BalloonTipIcon = ToolTipIcon.Info;
            TrayIcon.BalloonTipTitle = @"ZICrypt Messenger";
            TrayIcon.BalloonTipText = $@"Successfull login as {ZIController.Instance.Communicator.Username}";
            TrayIcon.ShowBalloonTip(2000);
        }
        private void ShowTrayIconNewOnlineUser(string username)
        {
            TrayIcon.BalloonTipIcon = ToolTipIcon.Info;
            TrayIcon.BalloonTipTitle = @"ZICrypt Messenger";
            TrayIcon.BalloonTipText = $@"New online user: {username}";
            TrayIcon.ShowBalloonTip(1500);
        }
        private void ShowTrayIconNewOfflineUser(string username)
        {
            TrayIcon.BalloonTipIcon = ToolTipIcon.Info;
            TrayIcon.BalloonTipTitle = @"ZICrypt Messenger";
            TrayIcon.BalloonTipText = $@"{username} went offline...";
            TrayIcon.ShowBalloonTip(1500);
        }
        private void ShowTrayIconNewMessage(string username, string message)
        {
            TrayIcon.BalloonTipIcon = ToolTipIcon.Info;
            TrayIcon.BalloonTipTitle = $@"{username} says:";
            TrayIcon.BalloonTipText = $@"{message}";
            TrayIcon.ShowBalloonTip(1500);
        }
        private void EnableMessaging()
        {
            textBoxUsername.Enabled = false;
            buttonNickname.Enabled = false;
            panelSendMessage.Enabled = true;

            ShowTrayIconSignedIn();
        }
        
        //todo: Refactor
        private void RefreshAvailableUsers()
        {
            void MethodWork()
            {
                BindingSource bs = new BindingSource();
                bs.DataSource = ZIController.Instance.Communicator.AvailableUsers;
                bs.ResetBindings(false);

                listBoxAvailableUsers.DataSource = bs;
                listBoxAvailableUsers.DisplayMember = "Username";
            }
            
            if (listBoxAvailableUsers.InvokeRequired)
                listBoxAvailableUsers.Invoke((MethodInvoker)MethodWork);
            else
                MethodWork();
        }
        private string FormatSentMessage(string message)
        {
            string formattedMessage =
                $"[{DateTime.Now}][SENT] ";

            foreach (Client communicatorReceiver in ZIController.Instance.Communicator.Receivers)
            {
                formattedMessage += $"[{communicatorReceiver.Username}] ";
            }

            formattedMessage += message + Environment.NewLine;
            return formattedMessage;
        }

        private void buttonNickname_Click(object sender, EventArgs e)
        {
            if (textBoxUsername.Text == string.Empty)
            {
                ShowMessage("Username cannot be emty", MessageBoxIcon.Error);
                return;
            }

            try
            {
                buttonNickname.Enabled = false;
                toolStripStatusMessage.Text = "Logging...";

                ZIController.Instance.Communicator.Username = textBoxUsername.Text;
                ZIController.Instance.Communicator.Login();
                ZIController.Instance.Communicator.OnOnlineClientsChange += CommunicatorOnOnlineClientsChange;
                ZIController.Instance.Communicator.OnNewMessage += CommunicatorOnOnNewMessage;

                EnableMessaging();
                RefreshAvailableUsers();
                toolStripStatusMessage.Text = "Successfull login.";

            }
            catch (Exception ex)
            {
                ShowMessage(ex.Message, MessageBoxIcon.Error);
                toolStripStatusMessage.Text = "Failed to login.";
            }
        }
        private void listBoxAvailableUsers_Click(object sender, EventArgs e)
        {
            ZIController.Instance.Communicator.Receivers = listBoxAvailableUsers.SelectedItems.Cast<Client>().ToList();
        }
        private void buttonSend_Click(object sender, EventArgs e)
        {
            if (textBoxMessage.Text == string.Empty)
                return;

            ZIController.Instance.Communicator.SendMessage(textBoxMessage.Text);

            string formattedMessage = FormatSentMessage(textBoxMessage.Text);
            textBoxReceivedMessages.AppendText(formattedMessage);

            textBoxMessage.Text = string.Empty;
        }

        //todo: Refactor
        private void CommunicatorOnOnNewMessage(object sender, MessageReceivedEventArgs e)
        {
            void MethodWorkShowMessage()
            {

                string formattedMessage =
                    $"[{DateTime.Now}] [{e.Message.Sender?.Username}] {e.Message.MessageText} {Environment.NewLine}";
                textBoxReceivedMessages.AppendText(formattedMessage);
            }

            void MethodWorkShowTrayIcon()
            {
                if (tabControl.SelectedTab != tabMessenger)
                    ShowTrayIconNewMessage(e.Message.Sender.Username, e.Message.MessageText);
            }


            if (textBoxReceivedMessages.InvokeRequired)
                textBoxConsoleFSW.Invoke((MethodInvoker)MethodWorkShowMessage);
            else
                MethodWorkShowMessage();

            if (tabControl.InvokeRequired)
                tabControl.Invoke((MethodInvoker)MethodWorkShowTrayIcon);

            else
                MethodWorkShowTrayIcon();

        }
        private void CommunicatorOnOnlineClientsChange(object sender, NewOnlineEventArgs e)
        {
            if (e.NewOnline.OnlineStatus == Client.Status.Online)
            {
                ShowTrayIconNewOnlineUser(e.NewOnline.Username);
            }
            else
            {
                ShowTrayIconNewOfflineUser(e.NewOnline.Username);
            }

            RefreshAvailableUsers();
        }
        #endregion

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            ZIController.Instance.FSW.Shutdown();
            ZIController.Instance.Communicator.SignOut();
        }
    }
}
